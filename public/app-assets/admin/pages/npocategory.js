$(function () {

    if($('#admin-categories-list-wrap').length) {

        const manageCategories = $('#table-manage-categories');
        if (manageCategories.length > 0) {

            $.extend( $.fn.dataTableExt.oStdClasses, {
                "sFilterInput": "form-control",
                "sLengthSelect": "form-control"
            });

            const tableManageCategories = manageCategories.DataTable({
                responsive: true,
                processing: true,
                serverSide: true,  
                autoWidth: false,  
                orderable: false,            
                ajax: {
                    url: manageCategories.data('url'),
                },
                columns: [                    
                    
                    {data: 'id', name: 'id'},
                    {data: 'category_image', name: 'category_image', orderable: false},
                    {data: 'category_name', name: 'category_name'},                    
                    {data: 'category_status', name: 'category_status', className: 'text-capitalize', orderable: false},                    
                    {data: 'action', className: 'text-center', orderable: false}
                ]
            });

           


            /* $(document.body).on('click','.act-delete-storelocations',function (e) {
                e.preventDefault();

                var url = $(this).attr('data-url');
                var name = $(this).attr('data-name');
                bootbox.confirm({
                    message: "Are you sure want to delete <strong>"+name+"</strong> store?",
                    centerVertical : true,
                    callback: function(result){
                        if(result){
                            window.location.href = url;
                        }
                        else {
                            return;
                        }
                    }
                })


            }); */


        }
    }

});