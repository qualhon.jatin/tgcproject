$(function () {




    if ($('#admin-giveaways-list-wrap').length) {

        const manageGiveaways = $('#table-manage-giveaways');
        if (manageGiveaways.length > 0) {

            $.extend($.fn.dataTableExt.oStdClasses, {
                "sFilterInput": "form-control",
                "sLengthSelect": "form-control"
            });

            const tableManageGiveaways = manageGiveaways.DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                autoWidth: false,
                orderable: false,
                ajax: {
                    url: manageGiveaways.data('url'),
                },
                columns: [

                    { data: 'id', name: 'id' },
                    { data: 'prize_title', name: 'prize_title', orderable: false },
                    { data: 'prize_start_date', name: 'prize_start_date' },
                    { data: 'prize_end_date', name: 'prize_end_date' },
                    { data: 'prize_status', name: 'prize_status', className: 'text-capitalize', orderable: false },
                    { data: 'action', className: 'text-center', orderable: false }
                ]
            });




        }
    }

});