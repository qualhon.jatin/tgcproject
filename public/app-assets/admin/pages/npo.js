$(function () {

    if ($('#admin-users-list-wrap').length) {

        const manageUsers = $('#table-manage-users');
        if (manageUsers.length > 0) {


            // Setup - add a text input to each footer cell
            $('#table-manage-users thead tr').clone(true).appendTo('#table-manage-users thead');
            $('#table-manage-users thead tr:eq(1) th').each(function (i) {

                if (i == 4) {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="Filter By ' + title + '" />');

                    $('input', this).on('keyup change', function () {
                        if (tableManageUsers.column(i).search() !== this.value) {
                            tableManageUsers
                                .column(i)
                                .search(this.value)
                                .draw();
                        }
                    });
                } else {
                    $(this).html('');
                }
            });


            $.extend($.fn.dataTableExt.oStdClasses, {
                "sFilterInput": "form-control",
                "sLengthSelect": "form-control"
            });

            const tableManageUsers = manageUsers.DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                fixedColumns: true,
                autoWidth: false,
                orderCellsTop: true,
                fixedHeader: false,
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excel',
                        text: 'Export as Excel',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    },
                    {
                        extend: 'csv',
                        text: 'Export as CSV',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    }
                ],
                ajax: {
                    url: manageUsers.data('url'),
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'organization_name', name: 'organization_name' },
                    { data: 'npo_first_name', name: 'npo_first_name' },
                    { data: 'npo_email', name: 'npo_email', orderable: false },
                    { data: 'category_name', name: 'category_name', orderable: false },
                    { data: 'npo_charity_navigator_rating', name: 'npo_charity_navigator_rating' },
                    { data: 'npo_phone', name: 'npo_phone', orderable: false },
                    { data: 'npo_city', name: 'npo_city', orderable: false },
                    { data: 'npo_state', name: 'npo_state', orderable: false },
                    { data: 'npo_status', name: 'npo_status', className: 'text-capitalize', orderable: false },
                    { data: 'action', className: 'text-center', orderable: false }
                ]
            });




            /* $(document.body).on('click','.act-delete-storelocations',function (e) {
                e.preventDefault();

                var url = $(this).attr('data-url');
                var name = $(this).attr('data-name');
                bootbox.confirm({
                    message: "Are you sure want to delete <strong>"+name+"</strong> store?",
                    centerVertical : true,
                    callback: function(result){
                        if(result){
                            window.location.href = url;
                        }
                        else {
                            return;
                        }
                    }
                })


            }); */


        }
    }

});



