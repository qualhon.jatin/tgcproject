$(function () {


    

    if($('#admin-coupons-list-wrap').length) {

        const manageCoupons = $('#table-manage-coupons');
        if (manageCoupons.length > 0) {

            $.extend( $.fn.dataTableExt.oStdClasses, {
                "sFilterInput": "form-control",
                "sLengthSelect": "form-control"
            });

            const tableManageCoupons = manageCoupons.DataTable({
                responsive: true,
                processing: true,
                serverSide: true,  
                autoWidth: false,  
                orderable: false,            
                ajax: {
                    url: manageCoupons.data('url'),
                },
                columns: [                    
                    
                    {data: 'id', name: 'id'},
                    {data: 'coupon_code', name: 'coupon_code', orderable: false},
                    {data: 'coupon_start_date', name: 'coupon_start_date', orderable: false},
                    {data: 'coupon_end_date', name: 'coupon_end_date', orderable: false},
                    {data: 'coupon_value', name: 'coupon_value', orderable: false},
                    {data: 'coupon_status', name: 'coupon_status', className: 'text-capitalize', orderable: false},                    
                    {data: 'action', className: 'text-center', orderable: false}
                ]
            });

           


        }
    }

});