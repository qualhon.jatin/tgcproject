<?php


/* --------------------- Common/User Routes START -------------------------------- */

Route::get('/', function () {
    // return view('welcome');
    return redirect('admin/dashboard');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

/* --------------------- Common/User Routes END -------------------------------- */

/* ----------------------- Admin Routes START -------------------------------- */

Route::prefix('/admin')->name('admin.')->namespace('Admin')->group(function () {

    /**
     * Admin Auth Route(s)
     */
    Route::namespace('Auth')->group(function () {

        //Login Routes
        Route::get('/login', 'LoginController@showLoginForm')->name('login');
        Route::post('/login', 'LoginController@login');
        Route::post('/logout', 'LoginController@logout')->name('logout');

        //Register Routes
        // Route::get('/register','RegisterController@showRegistrationForm')->name('register');
        // Route::post('/register','RegisterController@register');

        //Forgot Password Routes
        Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');

        //Reset Password Routes
        Route::get('/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('/password/reset', 'ResetPasswordController@reset')->name('password.update');

        // Email Verification Route(s)
        Route::get('email/verify', 'VerificationController@show')->name('verification.notice');
        Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');
        Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');
    });

    Route::get('/dashboard', 'HomeController@index')->name('home');
    Route::get('/settings', 'HomeController@settings')->name('settings');
    Route::post('/updateSettings', 'HomeController@updateSettings')->name('updateSettings');

    // User Management
    Route::get('/users', 'UserController@index')->name('user');
    Route::get('/getUserListings', 'UserController@getUserListings')->name('getUserListings');
    Route::get('/user/{id}', 'UserController@show')->name('user.show');
    Route::get('/user/{id}/edit', 'UserController@edit')->name('user.edit');
    Route::post('/user/{id}/edit', 'UserController@doEdit')->name('user.do-edit');
    Route::get('/user/{id}/delete', 'UserController@delete')->name('user.delete');

    // NPO Management
    Route::get('/npo-listings', 'NPOController@index')->name('npo');
    Route::get('/getNPOListings', 'NPOController@getNPOListings')->name('getNPOListings');
    Route::get('/add-npo', 'NPOController@add')->name('npo.add');
    Route::post('/addNpo', 'NPOController@addNpo')->name('npo.do-addNpo');
    Route::get('/npo/{id}', 'NPOController@show')->name('npo.show');
    Route::get('/npo/{id}/edit', 'NPOController@edit')->name('npo.edit');
    Route::post('/npo/{id}/edit', 'NPOController@doEdit')->name('npo.do-edit');
    Route::get('/npo/{id}/delete', 'NPOController@delete')->name('npo.delete');

    // NPO Categories
    Route::get('/npo-categories', 'NpoCategoriesController@index')->name('npocategories');
    Route::get('/getNPOCategoryListings', 'NpoCategoriesController@getNPOCategoryListings')->name('getNPOCategoryListings');
    Route::get('/add-npo-category', 'NpoCategoriesController@create')->name('npocategories.add-npo-category');
    Route::post('/createNpoCategory', 'NpoCategoriesController@createNpoCategory')->name('createNpoCategory');
    Route::get('/npo-categories/{id}/edit', 'NpoCategoriesController@edit')->name('npocategories.edit-npo-category');
    Route::post('/npo-categories/{id}/edit', 'NpoCategoriesController@updateNpoCategory')->name('updateNpoCategory');


    // NPO Coupons
    Route::get('/coupons', 'CouponController@index')->name('coupons');
    Route::get('/getCouponListings', 'CouponController@getCouponListings')->name('getCouponListings');
    Route::get('/add-coupon', 'CouponController@create')->name('coupons.add-coupon');
    Route::post('/createCoupon', 'CouponController@createCoupon')->name('createCoupon');
    Route::get('/coupons/{id}/edit', 'CouponController@edit')->name('coupons.edit-coupon');
    Route::post('/coupons/{id}/edit', 'CouponController@updateCoupon')->name('updateCoupon');   

    // Giveaways
    Route::get('/giveaways', 'GiveawaysController@index')->name('giveaways');
    Route::get('/getGiveawaysListings', 'GiveawaysController@getGiveawaysListings')->name('getGiveawaysListings');
    Route::get('/add-giveaway', 'GiveawaysController@create')->name('giveaways.add-giveaway');
    Route::post('/createGiveaway', 'GiveawaysController@createGiveaway')->name('createGiveaway');
    Route::get('/giveaways/{id}/edit', 'GiveawaysController@edit')->name('giveaways.edit-giveaway');
    Route::post('/giveaways/{id}/edit', 'GiveawaysController@updateGiveaway')->name('updateGiveaway'); 


    // Champions
    Route::get('/champions', 'ChampionsController@index')->name('champions');
    Route::get('/getChampionsListings', 'ChampionsController@getChampionsListings')->name('getChampionsListings');
    Route::get('/add-champion', 'ChampionsController@create')->name('champions.add-champion');
    Route::post('/createChampion', 'ChampionsController@createChampion')->name('createChampion');
    Route::get('/champions/{id}/edit', 'ChampionsController@edit')->name('champions.edit-champion');
    Route::post('/champions/{id}/edit', 'ChampionsController@updateChampion')->name('updateChampion'); 


    //Put all of your admin routes here...

});

/* ----------------------- Admin Routes END -------------------------------- */
