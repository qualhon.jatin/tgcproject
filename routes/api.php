<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!ss
|
*/
Route::prefix('v1/user')->group(function () {
    Route::post('register','API\V1\UserController@register');
    Route::post('login','API\V1\UserController@login');
    Route::post('forgotPassword','API\V1\UserController@forgotPassword');
    Route::post('verifyAuthToken','API\V1\UserController@verifyAuthToken');
    Route::post('resetPassword','API\V1\UserController@resetPassword');
    Route::group(['middleware' => 'auth:api'], function(){
        Route::post('logout','API\V1\UserController@logout');
        Route::get('getUserDetails','API\V1\UserController@getUserDetails');
        Route::post('updateUserProfile','API\V1\UserController@updateUserProfile');
        Route::post('addCart','API\V1\CartController@addCart');
        Route::get('getCartItems','API\V1\CartController@getCartItems');
        Route::post('deleteCartItem','API\V1\CartController@deleteCartItem');
        Route::post('updateCartItem','API\V1\CartController@updateCartItem');
        Route::get('applyCoupon','API\V1\CartController@applyCoupon');
        Route::post('makePayment','API\V1\PaymentController@makePayment');
        Route::get('getOrderHistory','API\V1\CartController@getOrderHistory');
    });
});
Route::prefix('v1/nonProfit')->group(function () {
    Route::post('register','API\V1\NonProfitController@register');
    Route::post('login','API\V1\NonProfitController@login');
    Route::post('forgotPassword','API\V1\NonProfitController@forgotPassword');
    Route::post('verifyAuthToken','API\V1\NonProfitController@verifyAuthToken');
    Route::post('resetPassword','API\V1\NonProfitController@resetPassword');    
    Route::group(['middleware' => 'auth:nonProfit'], function(){
        Route::post('logout','API\V1\NonProfitController@logout');
        Route::get('getNonProfitDetails','API\V1\NonProfitController@getNonProfitDetails');
        Route::post('updateNonProfitProfile','API\V1\NonProfitController@updateNonProfitProfile');
        Route::post('imageUploadtoBucket','API\V1\NonProfitController@imageUploadtoBucket');
    });
});

Route::prefix('v1')->group(function () {
    Route::get('prizes','API\V1\PrizeController@getPrizes');
    Route::get('prizeDetail/{id}','API\V1\PrizeController@getPrizeDetail');
    Route::get('getNPOCategories','API\V1\NonProfitController@getNPOCategories');
    Route::get('getNPOList','API\V1\NonProfitController@getNPOList');
    Route::get('getNPOListByCategory','API\V1\NonProfitController@getNPOListByCategory');

    //get prize exclude id
    Route::post('prizesexclude','API\V1\PrizeController@getPrizesExcludeId');
    

});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user(); 
});
