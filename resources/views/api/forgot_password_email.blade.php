<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>

<body>
    <table role="presentation" style="margin: 0 auto;max-width: 600px;font-family: 'open-sans',sans-serif;border-collapse: collapse;box-shadow: 0px 0px 10px #ccc; word-break: break-all;">
        <thead style="background: #f6f6f6;text-align: center;">
            <tr>
            <td style="padding: 18px 0;">
                <a style="color: #69cded;font-size: 22px;font-weight: 600;" href="#">THE GREAT CONNECT</a>
                </td>
            </tr>
        </thead>


        <tbody style="color:#2c2424;">
            <tr>
                <td style="padding: 40px 35px 15px;">
                    Hey {{$name}},
                    <br>
                    <br>
                    Here’s your forgot password <a href="{{$forgot_password_link}}">link</a>.
                    <br><br>
                    Let us know if you need anything else. &#128578;

                </td>
            </tr>

            <tr>
                <td style="padding:5px 35px;">
                    <p style="line-height: 1.7;">Stay Inspired!<br>
                        - Support Team
                    </p>
                </td>
            </tr>

        </tbody>

        <tfoot style="border-top: 1px solid #ddd; text-align: center;background: #f6f6f6;">
            <tr>
                <td style="padding: 20px 0px 5px 0px;">Follow The Great Connect</td>
            </tr>
            <tr>
                <td style="padding: 5px 0px;">
                    <table style="display: inline-block">
                        <tbody>
                            <tr>
                                <td style="padding-right: 10px"><a href="javascript:void(0)"><img src="https://marketing-image-production.s3.amazonaws.com/uploads/76628631887fa9ed4f9103ee8288c7fa38eaa8ab75cb569014035bf0058835394888398650a1015199d8669acba6ca2d0422a2b58aa6dd58bb21b79281c625a4.png"></a></td>
                                <td style="padding-right: 10px"><a href="javascript:void(0)"><img src="https://marketing-image-production.s3.amazonaws.com/uploads/2742f26687aaab273c4469555b7c9f85c3aa078647d1f275c004a67aef4207af7cf2f8c0ec7d3eb492b3c262f9a830f48cee65f62c2894619b77c0cbefa2d055.png"></a></td>

                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>

            <tr>
                <td style="padding:0 20px;font-size: 14px;">
                    <p>Copyright © 2020 The Great Connect</p>
                </td>
            </tr>
            <tr>
                <td style="padding:0px 20px;">
                <a style="color: #69cded;font-size: 17px;font-weight: 600;" href="#">THE GREAT CONNECT</a>
                </td>
            </tr>

            

        </tfoot>

    </table>

</body>

</html>