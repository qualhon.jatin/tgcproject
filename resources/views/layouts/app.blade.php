<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('app-assets/admin/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('app-assets/admin/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('app-assets/admin/dist/css/developer.css')}}">

    <!-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> -->

    @include('admin.includes.header')

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        /*
            these styles will animate bootstrap alerts.
        */
        .alert {
            z-index: 99;
            top: 60px;
            right: 18px;
            min-width: 30%;
            position: fixed;
            animation: slide 0.5s forwards;
        }

        @keyframes slide {
            100% {
                top: 30px;
            }
        }

        @media screen and (max-width: 668px) {
            .alert {
                /* center the alert on small screens */
                left: 10px;
                right: 10px;
            }
        }
    </style>

    <title>{{config('app.name')}}</title>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        @include('inc.navbar')

        @yield('content')
        @if(Auth::guard('admin')->check())
        <footer class="main-footer">
            <strong>Copyright &copy; 2020 <a href="javascript:void(0)">TGC</a>.</strong>
            All rights reserved.

        </footer>
        @endif
        <!-- jQuery -->
        <script src="{{asset('app-assets/admin/plugins/jquery/jquery.min.js')}}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{asset('app-assets/admin/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button)
        </script>
        <!-- Bootstrap 4 -->
        <script src="{{asset('app-assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        
        <!-- AdminLTE App -->
        <script src="{{asset('app-assets/admin/dist/js/adminlte.js')}}"></script>
        @include('admin.includes.footer')



        {{-- Success Alert --}}
        @if(session('status'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{session('status')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        {{-- Error Alert --}}
        @if(session('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{session('error')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif



        <script>
            //close the alert after 3 seconds.
            $(document).ready(function() {
                setTimeout(function() {
                    $(".alert").alert('close');
                }, 3000);
            });
        </script>
    </div>
</body>

</html>