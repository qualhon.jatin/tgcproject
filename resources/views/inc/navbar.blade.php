@if(Auth::guard('admin')->check())
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <div class="sidebar">

        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            
            <div class="info">
                <a href="#" class="d-block">Hello {{ Auth::guard('admin')->user()->name }}</a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                


                <li class="nav-item">
                    <a href="{{ route('admin.home') }}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.user') }}" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>Users</p>
                    </a>
                </li>


                <li class="nav-item has-treeview">
                    <a href="javascript:void(0)" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Non Profits
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.npo') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All Non Profits</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.npo.add') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add New</p>
                            </a>
                        </li> 
                        <li class="nav-item has-treeview">
                            <a href="javascript:void(0)" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>
                                    Categories
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('admin.npocategories') }}" class="nav-link">
                                        <i class="far fa-dot-circle nav-icon"></i>
                                        <p>All Categories</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.npocategories.add-npo-category') }}" class="nav-link">
                                        <i class="far fa-dot-circle nav-icon"></i>
                                        <p>Add New</p>
                                    </a>
                                </li>


                            </ul>
                        </li>

                    </ul>
                </li>


                <li class="nav-item">
                    <a href="{{ route('admin.giveaways') }}" class="nav-link">
                        <i class="nav-icon fas fa-award"></i>
                        <p>Giveaways</p>
                    </a>
                </li>


                <li class="nav-item">
                    <a href="{{ route('admin.champions') }}" class="nav-link">
                        <i class="nav-icon fas fa-award"></i>
                        <p>Champions</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.coupons') }}" class="nav-link">
                        <i class="nav-icon fas fa-tags"></i>
                        <p>Coupons</p>
                    </a>
                </li>

              

                <li class="nav-item">
                    <a href="{{ route('admin.settings') }}" class="nav-link">
                        <i class="nav-icon fas fa-user-cog"></i>
                        <p>Settings</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="javasript:void(0)" onclick="event.preventDefault();
                                                document.querySelector('#admin-logout-form').submit();" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>Sign Out</p>
                    </a>

                    <form id="admin-logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>



            </ul>
        </nav>

    </div>
</aside>
@endif