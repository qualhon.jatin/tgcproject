<div class="text-center">    

    <a class="btn btn-info btn-sm" href="{{ route('admin.giveaways.edit-giveaway', $id) }}">
        <i class="fas fa-pencil-alt">
        </i>
        Edit
    </a>   
</div>