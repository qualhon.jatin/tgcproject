@extends('layouts.app')

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('app-assets/admin/plugins/jquery-ui/jquery-ui.css')}}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Giveaway</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add Giveaway</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">

                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data" action="{{ route('admin.createGiveaway') }}">
                            {{ csrf_field() }}


                            <div class="form-group">
                                <label for="inputName">Upload Image / Video 1 &nbsp;
                                    <input type="file" name="prize_image_or_video_1" id="prize_image_or_video_1" class="{{ ( $errors->has('prize_image_or_video_1') ) ? 'is-invalid' : '' }}" value="">

                                    @if($errors->has('prize_image_or_video_1'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('prize_image_or_video_1') }}
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Upload Image / Video 2 &nbsp;
                                    <input type="file" name="prize_image_or_video_2" id="prize_image_or_video_2" class="{{ ( $errors->has('prize_image_or_video_2') ) ? 'is-invalid' : '' }}" value="">

                                    @if($errors->has('prize_image_or_video_2'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('prize_image_or_video_2') }}
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Start Date</label>
                                <input type="text" name="prize_start_date" id="inputPrizeStartDate" class="form-control {{ $errors->has('prize_start_date') ? 'is-invalid' : '' }}" value="{{ old('prize_start_date') }}">

                                @if($errors->has('prize_start_date'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('prize_start_date') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">End Date</label>
                                <input type="text" name="prize_end_date" id="inputPrizeEndDate" class="form-control {{ $errors->has('prize_end_date') ? 'is-invalid' : '' }}" value="{{ old('prize_end_date') }}">

                                @if($errors->has('prize_end_date'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('prize_end_date') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Sponser
                                    <input type="file" name="prize_sponser_image" id="prize_sponser_image" class="{{ ( $errors->has('prize_sponser_image') ) ? 'is-invalid' : '' }}" value="">

                                    @if($errors->has('prize_sponser_image'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('prize_sponser_image') }}
                                    </span>
                                    @endif
                            </div>

                            <div id="dynamic_field" class="form-group">


                                <label for="inputName">Entry Value</label>
                                <input type="number" name="entry_value" id="inputEntryValue" class="form-control {{ $errors->has('entry_value_array') ? 'is-invalid' : '' }}" value="">



                                <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                                <input type="hidden" name="entry_value_array" id="entry_value_array" value="{{ old('entry_value_array') }}">

                                @if($errors->has('entry_value_array'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('entry_value_array') }}
                                </span>
                                @endif

                            </div>
                            <div id="showResponse" class="form-group">
                                @if(old('entry_value_array'))
                                @foreach(json_decode(old('entry_value_array')) as $key => $Value)
                                <button type="button" name="remove" id="{{$key}}" class="btn btn-danger btn_remove">X</button>

                                <label for="inputName">Entry Value {{$key+1}}: </label>
                                <span class="entry_title">{{$Value->entry_value}}</span>
                                <br />
                                @endforeach
                                @endif
                            </div>

                            



                            <div class="form-group">
                                <label for="inputDescription">Heading</label>
                                <textarea rows="2" name="prize_title" id="prize_title" class="form-control {{ ( $errors->has('prize_title') ) ? 'is-invalid' : '' }}">{{ old('prize_title') }}</textarea>
                                @if($errors->has('prize_title'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('prize_title') }}
                                </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <label for="inputDescription">Sub Title</label>
                                <textarea rows="2" name="prize_sub_title" id="prize_sub_title" class="form-control {{ ( $errors->has('prize_sub_title') ) ? 'is-invalid' : '' }}">{{ old('prize_sub_title') }}</textarea>
                                @if($errors->has('prize_sub_title'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('prize_sub_title') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputDescription">Body Text</label>
                                <textarea rows="8" name="prize_body_text" id="prize_body_text" class="form-control {{ ( $errors->has('prize_body_text') ) ? 'is-invalid' : '' }}">{{ old('prize_body_text') }}</textarea>
                                @if($errors->has('prize_body_text'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('prize_body_text') }}
                                </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <label for="inputDescription">Disclaimer Text</label>
                                <textarea rows="8" name="prize_disclaimer_text" id="prize_disclaimer_text" class="form-control {{ ( $errors->has('prize_disclaimer_text') ) ? 'is-invalid' : '' }}">{{ old('prize_disclaimer_text') }}</textarea>
                                @if($errors->has('prize_disclaimer_text'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('prize_disclaimer_text') }}
                                </span>
                                @endif
                            </div>

                            <div class="col-12">
                                <a href="{{ route('admin.giveaways') }}" class="btn btn-secondary">Cancel</a>
                                <input type="hidden" name="prize_status" value="active">
                                <input type="submit" value="Add" class="btn btn-success float-right">
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('footer')
<script>
    var custom_event_values_array;
    <?php if (old('entry_value_array')) { ?>
        custom_event_values_array = JSON.parse('<?= old('entry_value_array'); ?>');
    <?php } else { ?>
        custom_event_values_array = [];
    <?php } ?>
    $(function() {



        $('#add').click(function() {

            eventValue = jQuery('#inputEntryValue').val();
            if (eventValue != "") {
                custom_event_values_array.push({
                    entry_value: eventValue
                });

                performArray(custom_event_values_array);
                jQuery('#inputEntryValue').val('');

            } else {
                alert('Please enter entry value');
            }

        });

        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");
            custom_event_values_array.splice(button_id, 1);
            performArray(custom_event_values_array);


            //$('#row' + button_id + '').remove();
        });

        function performArray(custom_event_values_array) {
            printResponse = '';
            printResponse += '<div class="form-group dynamic-added"> ';

            custom_event_values_array.forEach(function(arrayItem, index) {
                printResponse += '<button type="button" name="remove" id="' + (index) + '" class="btn btn-danger btn_remove">X</button><label for="inputName">Entry Value ' + (index + 1) + ': </label><span class="entry_title">' + arrayItem.entry_value + '</span><br/>';
            });

            printResponse += '</div>';
            jQuery("div#showResponse").html(printResponse);

            var data;
            if(custom_event_values_array.length > 0){
                data = JSON.stringify(custom_event_values_array);
            }else{
                data = '';
            }
            
            jQuery("input#entry_value_array").val(data);
        }


        $("#inputPrizeStartDate").datepicker({
            minDate: 0,
            dateFormat: 'yy-mm-dd',
            onSelect: function(selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $("#inputPrizeEndDate").datepicker("option", "minDate", dt);
            }
        });
        $("#inputPrizeEndDate").datepicker({
            minDate: 0,
            dateFormat: 'yy-mm-dd',
            onSelect: function(selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() - 1);
                $("#inputPrizeStartDate").datepicker("option", "maxDate", dt);
            }
        });

    });
</script>

@endsection