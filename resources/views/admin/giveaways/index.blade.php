@extends('layouts.app')


@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('app-assets/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('app-assets/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endsection

@section('content')


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Giveaways</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Giveaways</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2 float-sm-right">
                    <a class="btn btn-block btn-info " href="{{ route('admin.giveaways.add-giveaway') }}">
                        {{ __('Add New Giveaway') }}
                    </a>

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-body" id="admin-giveaways-list-wrap">
                        <table id="table-manage-giveaways" class="table table-bordered table-striped" data-url="{{ route('admin.getGiveawaysListings') }}">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Prize Name</th>                                    
                                    <th>Start Date</th>                                    
                                    <th>End Date</th>                                    
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->



@endsection

@section('footer')
<!-- DataTables -->
<script src="{{asset('app-assets/admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('app-assets/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('app-assets/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('app-assets/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('app-assets/admin/pages/giveaways.js')}}"></script>
@endsection