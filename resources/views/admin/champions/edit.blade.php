@extends('layouts.app')

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('app-assets/admin/plugins/jquery-ui/jquery-ui.css')}}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Champion</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Edit Champion</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">

                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data" action="{{ route('admin.updateChampion', $championById->id) }}">
                            {{ csrf_field() }}


                            <div class="form-group">
                                <label for="inputName">Upload Image / Video 1 &nbsp;
                                    <input type="file" name="prize_image_or_video_1" id="prize_image_or_video_1" class="{{ ( $errors->has('prize_image_or_video_1') ) ? 'is-invalid' : '' }}" value="">

                                    @if($errors->has('prize_image_or_video_1'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('prize_image_or_video_1') }}
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group">
                                <?php $imageArray = array('jpeg', 'png', 'jpg', 'gif'); ?>
                                <?php $videoArray = array('mp4', 'mov', 'flv', 'm3u8', 'ts', '3gp', 'avi', 'wmv'); ?>

                                @if (in_array(pathinfo($championById->prize_image_or_video_1, PATHINFO_EXTENSION), $imageArray))
                                <img src="{{ asset($championById->prize_image_or_video_1) }}" style="width: 100px; height: 100px;">
                                @endif

                                @if (in_array(pathinfo($championById->prize_image_or_video_1, PATHINFO_EXTENSION), $videoArray))

                                <video width="320" height="240" controls>
                                    <source src="{{ asset($championById->prize_image_or_video_1) }}" type="video/<?php echo pathinfo($championById->prize_image_or_video_1, PATHINFO_EXTENSION); ?>">                          
                                </video>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Upload Image / Video 2 &nbsp;
                                    <input type="file" name="prize_image_or_video_2" id="prize_image_or_video_2" class="{{ ( $errors->has('prize_image_or_video_2') ) ? 'is-invalid' : '' }}" value="">

                                    @if($errors->has('prize_image_or_video_2'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('prize_image_or_video_2') }}
                                    </span>
                                    @endif
                            </div>


                            <div class="form-group">
                                <?php $imageArray = array('jpeg', 'png', 'jpg', 'gif'); ?>
                                <?php $videoArray = array('mp4', 'mov', 'flv', 'm3u8', 'ts', '3gp', 'avi', 'wmv'); ?>

                                @if (in_array(pathinfo($championById->prize_image_or_video_2, PATHINFO_EXTENSION), $imageArray))
                                <img src="{{ asset($championById->prize_image_or_video_2) }}" style="width: 100px; height: 100px;">
                                @endif

                                @if (in_array(pathinfo($championById->prize_image_or_video_2, PATHINFO_EXTENSION), $videoArray))

                                <video width="320" height="240" controls>
                                    <source src="{{ asset($championById->prize_image_or_video_2) }}" type="video/<?php echo pathinfo($championById->prize_image_or_video_2, PATHINFO_EXTENSION); ?>">                          
                                </video>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Start Date</label>
                                <input type="text" name="prize_start_date" id="inputPrizeStartDate" class="form-control {{ $errors->has('prize_start_date') ? 'is-invalid' : '' }}" value="{{ old('prize_start_date', $championById->prize_start_date) }}">

                                @if($errors->has('prize_start_date'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('prize_start_date') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">End Date</label>
                                <input type="text" name="prize_end_date" id="inputPrizeEndDate" class="form-control {{ $errors->has('prize_end_date') ? 'is-invalid' : '' }}" value="{{ old('prize_end_date', $championById->prize_end_date) }}">

                                @if($errors->has('prize_end_date'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('prize_end_date') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Non Profit Partner
                                <select name="prize_nonprofit_id" class="form-control custom-select {{ ( $errors->has('prize_nonprofit_id') ) ? 'is-invalid' : '' }}">
                                    <option selected disabled>Select one</option>
                                    @foreach($getNpos as $getNpo)
                                    <option {{ old( 'prize_nonprofit_id', $getNpo->id ) == $championById->prize_nonprofit_id ? 'selected' : '' }} value="{{ $getNpo->id }}">{{ $getNpo->organization_name }}</option>
                                    @endforeach
                                </select>

                                    @if($errors->has('prize_nonprofit_id'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('prize_nonprofit_id') }}
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Sponser
                                    <input type="file" name="prize_sponser_image" id="prize_sponser_image" class="{{ ( $errors->has('prize_sponser_image') ) ? 'is-invalid' : '' }}" value="">

                                    @if($errors->has('prize_sponser_image'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('prize_sponser_image') }}
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group">
                                @if ($championById->prize_sponser_image)
                                <img src="{{ asset($championById->prize_sponser_image) }}" style="width: 100px; height: 100px;">
                                @endif
                            </div>

                            <div id="dynamic_field" class="form-group">


                                <label for="inputName">Entry Value: </label>
                                <input type="number" name="entry_value" id="inputEntryValue" class="form-control {{ $errors->has('entry_value_array') ? 'is-invalid' : '' }}" value="">



                                <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                                <input type="hidden" name="entry_value_array" id="entry_value_array" value="{{ old('entry_value_array', $championById->prize_entry_values) }}">

                                @if($errors->has('entry_value_array'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('entry_value_array') }}
                                </span>
                                @endif

                            </div>
                            <div id="showResponse" class="form-group">
                                @if(old('entry_value_array', $championById->prize_entry_values))
                                @foreach(json_decode(old('entry_value_array', $championById->prize_entry_values)) as $key => $Value)
                                <button type="button" name="remove" id="{{$key}}" class="btn btn-danger btn_remove">X</button>
                                <label for="inputName">Entry Value {{$key+1}}: </label>

                                <span class="entry_title">{{$Value->entry_value}}</span>
                                <br />
                                @endforeach
                                @endif
                            </div>



                            <div class="form-group">
                                <label for="inputDescription">Heading</label>
                                <textarea rows="2" name="prize_title" id="prize_title" class="form-control {{ ( $errors->has('prize_title') ) ? 'is-invalid' : '' }}">{{ old('prize_title', $championById->prize_title) }}</textarea>
                                @if($errors->has('prize_title'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('prize_title') }}
                                </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <label for="inputDescription">Sub Title</label>
                                <textarea rows="2" name="prize_sub_title" id="prize_sub_title" class="form-control {{ ( $errors->has('prize_sub_title') ) ? 'is-invalid' : '' }}">{{ old('prize_sub_title', $championById->prize_sub_title) }}</textarea>
                                @if($errors->has('prize_sub_title'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('prize_sub_title') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputDescription">Body Text</label>
                                <textarea rows="8" name="prize_body_text" id="prize_body_text" class="form-control {{ ( $errors->has('prize_body_text') ) ? 'is-invalid' : '' }}">{{ old('prize_body_text', $championById->prize_body_text) }}</textarea>
                                @if($errors->has('prize_body_text'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('prize_body_text') }}
                                </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <label for="inputDescription">Disclaimer Text</label>
                                <textarea rows="8" name="prize_disclaimer_text" id="prize_disclaimer_text" class="form-control {{ ( $errors->has('prize_disclaimer_text') ) ? 'is-invalid' : '' }}">{{ old('prize_disclaimer_text', $championById->prize_disclaimer_text) }}</textarea>
                                @if($errors->has('prize_disclaimer_text'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('prize_disclaimer_text') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputStatus">Status</label>
                                <select name="prize_status" class="form-control custom-select">
                                    <option selected disabled>Select one</option>
                                    <option {{ old( 'prize_status', $championById->prize_status ) == 'active' ? 'selected' : '' }} value="active">Active</option>
                                    <option {{ old( 'prize_status', $championById->prize_status ) == 'inactive' ? 'selected' : '' }} value="inactive">Inactive</option>
                                </select>
                            </div>

                            <div class="col-12">
                                <a href="{{ route('admin.champions') }}" class="btn btn-secondary">Cancel</a>
                                <input type="submit" value="Update" class="btn btn-success float-right">
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('footer')
<script>
    var custom_event_values_array;
    <?php if (old('entry_value_array', $championById->prize_entry_values)) { ?>
        custom_event_values_array = JSON.parse('<?= old('entry_value_array', $championById->prize_entry_values); ?>');
    <?php } else { ?>
        custom_event_values_array = [];
    <?php } ?>
    $(function() {



        $('#add').click(function() {

           // console.log(custom_event_values_array); return false;

            eventValue = jQuery('#inputEntryValue').val();
            if (eventValue != "") {
                custom_event_values_array.push({
                    entry_value: eventValue
                });

                performArray(custom_event_values_array);
                jQuery('#inputEntryValue').val('');

            } else {
                alert('Please enter entry value');
            }

        });

        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");
            custom_event_values_array.splice(button_id, 1);
            performArray(custom_event_values_array);


            //$('#row' + button_id + '').remove();
        });

        function performArray(custom_event_values_array) {
            printResponse = '';
            printResponse += '<div class="form-group dynamic-added"> ';

            custom_event_values_array.forEach(function(arrayItem, index) {
                printResponse += '<button type="button" name="remove" id="' + (index) + '" class="btn btn-danger btn_remove">X</button><label for="inputName">Entry Value ' + (index + 1) + ': </label><span class="entry_title">' + arrayItem.entry_value + '</span><br/>';
            });

            printResponse += '</div>';
            jQuery("div#showResponse").html(printResponse);

            var data;
            if(custom_event_values_array.length > 0){
                data = JSON.stringify(custom_event_values_array);
            }else{
                data = '';
            }
            
            jQuery("input#entry_value_array").val(data);
        }


        $("#inputPrizeStartDate").datepicker({
            minDate: 0,
            dateFormat: 'yy-mm-dd',
            onSelect: function(selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $("#inputPrizeEndDate").datepicker("option", "minDate", dt);
            }
        });
        $("#inputPrizeEndDate").datepicker({
            minDate: 0,
            dateFormat: 'yy-mm-dd',
            onSelect: function(selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() - 1);
                $("#inputPrizeStartDate").datepicker("option", "maxDate", dt);
            }
        });

    });
</script>

@endsection