@extends('layouts.app')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Settings</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Settings</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">

                    <div class="card-body">
                        <form method="post" action="{{ route('admin.updateSettings') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="inputName">Name</label>
                                <input type="text" name="name" id="inputCategoryName" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name', Auth::guard('admin')->user()->name) }}">

                                @if($errors->has('name'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Email</label>
                                <input type="email" readonly name="email" id="inputCategoryName" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ old('email', Auth::guard('admin')->user()->email) }}">

                                @if($errors->has('email'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">New Password</label>
                                <input type="password" name="password" id="inputCategoryPassword" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" value="{{ old('password') }}">

                                @if($errors->has('password'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Confirm Password</label>
                                <input type="password" name="password_confirmation" id="inputCategoryPassword" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" value="{{ old('password_confirmation') }}">

                                @if($errors->has('password_confirmation'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('password_confirmation') }}
                                </span>
                                @endif
                            </div>


                            <div class="col-12">
                                <input type="submit" value="Update" class="btn btn-success float-right">
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection