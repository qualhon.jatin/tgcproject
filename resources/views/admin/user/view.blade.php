@extends('layouts.app')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>User View</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">{{ $userById->name }} / Edit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    
                    <div class="card-body">
                        <form method="post" action="{{ route('admin.user.do-edit', $userById->id) }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="inputName">First Name</label>
                                <input type="text" name="first_name" id="inputFirstName" class="form-control {{ ( $errors->has('first_name') || $errors->has('invalidUserUpdate') ) ? 'is-invalid' : '' }}" value="{{ old('first_name',$userById->first_name) }}">

                                @if($errors->has('first_name'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('first_name') }}
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputName">Last Name</label>
                                <input type="text" name="last_name" id="inputLastName" class="form-control {{ ( $errors->has('last_name') || $errors->has('invalidUserUpdate') ) ? 'is-invalid' : '' }}" value="{{ old('last_name',$userById->last_name) }}">

                                @if($errors->has('last_name'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('last_name') }}
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputDescription">Email</label>
                                <input type="text" name="email" id="inputEmail" class="form-control {{ ( $errors->has('email') || $errors->has('invalidUserUpdate') ) ? 'is-invalid' : '' }}" value="{{ old('email',$userById->email) }}" readonly>

                                @if($errors->has('email'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputDescription">New Password</label>
                                <input type="password" name="password" id="inputPassword" class="form-control {{ ( $errors->has('password') || $errors->has('invalidUserUpdate') ) ? 'is-invalid' : '' }}" value="">
                                @if($errors->has('password'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputDescription">Confirm Password</label>
                                <input type="password" name="password_confirmation" id="inputConfirmPassword" class="form-control {{ ( $errors->has('password_confirmation') ) ? 'is-invalid' : '' }}" value="">
                                @if($errors->has('password_confirmation'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('password_confirmation') }}
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputStatus">Status</label>
                                <select name="user_status" class="form-control custom-select">
                                    <option selected disabled>Select one</option>
                                    <option {{ old( 'user_status', $userById->user_status ) == 'active' ? 'selected' : '' }} value="active">Active</option>
                                    <option {{ old( 'user_status', $userById->user_status ) == 'inactive' ? 'selected' : '' }} value="inactive">Inactive</option>
                                </select>
                            </div>

                            <div class="col-12">
                                <a href="{{ route('admin.user') }}" class="btn btn-secondary">Cancel</a>
                                <input type="submit" value="Update" class="btn btn-success float-right">
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection