<div class="text-center">
    <?php /* <a class="btn btn-primary btn-sm" href="javascript:void(0)">
        <i class="fas fa-folder">
        </i>
        View Donations
    </a> */ ?>

    <a class="btn btn-info btn-sm" href="{{ route('admin.npo.edit', $id) }}">
        <i class="fas fa-pencil-alt">
        </i>
        Edit
    </a>
    
    <?php /* <a data-name="{{ $first_name }} {{ $last_name }}" data-url="{{ route('admin.user.delete', $id)  }}" href="#" class="btn icon-btn btn-sm btn-outline-primary act-delete-user"> <i class="fa fa-trash-alt"></i> </a> */ ?>
</div>