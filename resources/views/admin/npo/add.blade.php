@extends('layouts.app')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add NPO</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add NPO</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">

                    <div class="card-body">
                        <form enctype="multipart/form-data" method="post" action="{{ route('admin.npo.do-addNpo') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="inputName">Upload Logo
                                    <input type="file" name="npo_logo" id="npo_logo" class="{{ ( $errors->has('npo_logo') ) ? 'is-invalid' : '' }}" value="">

                                    @if($errors->has('npo_logo'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('npo_logo') }}
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Upload Image / Video
                                    <input type="file" name="npo_image_or_video" id="npo_image_or_video" class="{{ ( $errors->has('npo_image_or_video') ) ? 'is-invalid' : '' }}" value="">

                                    @if($errors->has('npo_image_or_video'))
                                    <span class="invalid-feedback">
                                        {{ $errors->first('npo_image_or_video') }}
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Organization Name</label>
                                <input type="text" name="organization_name" id="organization_name" class="form-control {{ ( $errors->has('organization_name') ) ? 'is-invalid' : '' }}" value="{{ old('organization_name') }}">

                                @if($errors->has('organization_name'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('organization_name') }}
                                </span>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputDescription">First Name</label>
                                        <input type="text" name="npo_first_name" id="npo_first_name" class="form-control {{ ( $errors->has('npo_first_name') ) ? 'is-invalid' : '' }}" value="{{ old('npo_first_name') }}">
                                        @if($errors->has('npo_first_name'))
                                        <span class="invalid-feedback">
                                            {{ $errors->first('npo_first_name') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputName">Last Name</label>
                                        <input type="text" name="npo_last_name" id="npo_last_name" class="form-control npo_last_name {{ ( $errors->has('npo_last_name') ) ? 'is-invalid' : '' }}" value="{{ old('npo_last_name') }}">

                                        @if($errors->has('npo_last_name'))
                                        <span class="invalid-feedback">
                                            {{ $errors->first('npo_last_name') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <label for="inputDescription">Email</label>
                                <input type="email" name="npo_email" id="npo_email" class="form-control {{ ( $errors->has('npo_email') ) ? 'is-invalid' : '' }}" value="{{ old('npo_email') }}">

                                @if($errors->has('npo_email'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('npo_email') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputDescription">Phone</label>
                                <input type="tel" name="npo_phone" id="npo_phone" class="form-control npo_phone {{ ( $errors->has('npo_phone') ) ? 'is-invalid' : '' }}" value="{{ old('npo_phone') }}">

                                @if($errors->has('npo_phone'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('npo_phone') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputDescription">Password</label>
                                <input type="password" name="password" id="password" class="form-control {{ ( $errors->has('password') ) ? 'is-invalid' : '' }}" value="">
                                @if($errors->has('password'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="inputDescription">City</label>
                                        <input type="text" name="npo_city" id="npo_city" class="form-control {{ ( $errors->has('npo_city') ) ? 'is-invalid' : '' }}" value="{{ old('npo_city') }}">
                                        @if($errors->has('npo_city'))
                                        <span class="invalid-feedback">
                                            {{ $errors->first('npo_city') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="inputDescription">State</label>
                                        <input type="text" name="npo_state" id="npo_state" class="form-control {{ ( $errors->has('npo_state') ) ? 'is-invalid' : '' }}" value="{{ old('npo_state') }}">
                                        @if($errors->has('npo_state'))
                                        <span class="invalid-feedback">
                                            {{ $errors->first('npo_state') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="inputDescription">Zip Code</label>
                                        <input type="text" name="npo_zipcode" id="npo_zipcode" class="form-control npo_zipcode {{ ( $errors->has('npo_zipcode') ) ? 'is-invalid' : '' }}" value="{{ old('npo_zipcode') }}">
                                        @if($errors->has('npo_zipcode'))
                                        <span class="invalid-feedback">
                                            {{ $errors->first('npo_zipcode') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputDescription">Website Url</label>
                                <input type="text" name="npo_website_url" id="npo_website_url" class="form-control {{ ( $errors->has('npo_website_url') ) ? 'is-invalid' : '' }}" value="{{ old('npo_website_url') }}">
                                @if($errors->has('npo_website_url'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('npo_website_url') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputDescription">Category</label>


                                <select name="npo_category" class="form-control custom-select {{ ( $errors->has('npo_category') ) ? 'is-invalid' : '' }}">
                                    <option selected disabled>Select one</option>
                                    @foreach($getNpoCategories as $getNpoCategory)
                                    <option value="{{ $getNpoCategory->id }}"@if($getNpoCategory->id == old('npo_category')) selected @endif >{{ $getNpoCategory->category_name }}</option>
                                    @endforeach
                                </select>

                                @if($errors->has('npo_category'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('npo_category') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputDescription">Charity Navigator Rating</label>

                                <select name="npo_charity_navigator_rating" class="form-control custom-select {{ ( $errors->has('npo_charity_navigator_rating') ) ? 'is-invalid' : '' }}">
                                    <option selected disabled>Select one</option>
                                    @for ($i = 1; $i <= 4; $i++) <option value="{{ $i }}" @if( $i == old('npo_charity_navigator_rating')) selected @endif >{{ $i }}</option>
                                        @endfor
                                        <option value="N/A">N/A</option>
                                </select>

                                @if($errors->has('npo_charity_navigator_rating'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('npo_charity_navigator_rating') }}
                                </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <label for="inputDescription">Mission Statement</label>
                                <textarea rows="8" name="npo_mission_statement" id="npo_mission_statement" class="form-control {{ ( $errors->has('npo_mission_statement') ) ? 'is-invalid' : '' }}">{{ old('npo_mission_statement') }}</textarea>
                                @if($errors->has('npo_mission_statement'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('npo_mission_statement') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputDescription">About Us / Our Impact</label>
                                <textarea rows="8" name="npo_about_us" id="npo_about_us" class="form-control {{ ( $errors->has('npo_about_us') ) ? 'is-invalid' : '' }}">{{ old('npo_about_us') }}</textarea>
                                @if($errors->has('npo_about_us'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('npo_about_us') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputDescription">Celebrity or Influential Supporters include</label>
                                <textarea rows="8" name="npo_celebrity_or_influential_supporters_include" id="npo_celebrity_or_influential_supporters_include" class="form-control {{ ( $errors->has('npo_celebrity_or_influential_supporters_include') ) ? 'is-invalid' : '' }}">{{ old('npo_celebrity_or_influential_supporters_include') }}</textarea>
                                @if($errors->has('npo_celebrity_or_influential_supporters_include'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('npo_celebrity_or_influential_supporters_include') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputDescription">Major Corporate Partners / Sponsors Include</label>
                                <textarea rows="8" name="npo_major_coporate_partners_include" id="npo_major_coporate_partners_include" class="form-control {{ ( $errors->has('npo_major_coporate_partners_include') ) ? 'is-invalid' : '' }}">{{ old('npo_major_coporate_partners_include') }}</textarea>
                                @if($errors->has('npo_major_coporate_partners_include'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('npo_major_coporate_partners_include') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputDescription">Affiliate Code</label>
                                <input type="text" name="npo_affiliate_code" id="npo_affiliate_code" class="form-control {{ ( $errors->has('npo_affiliate_code') ) ? 'is-invalid' : '' }}" value="{{ old('npo_affiliate_code') }}">
                                @if($errors->has('npo_affiliate_code'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('npo_affiliate_code') }}
                                </span>
                                @endif
                            </div>


                            <div class="col-12">
                                <a href="{{ route('admin.npo') }}" class="btn btn-secondary">Cancel</a>
                                <input type="submit" value="Add" class="btn btn-success float-right">
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('footer')
<script src="{{asset('app-assets/admin/pages/jquery.maskedinput.js')}}"></script>
<script src="{{asset('app-assets/admin/pages/addnpo.js')}}"></script>
@endsection