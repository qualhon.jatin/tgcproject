@extends('layouts.app')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Category</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add Category</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    
                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data" action="{{ route('admin.createNpoCategory') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="inputName">Category Name</label>
                                <input type="text" name="category_name" id="inputCategoryName" class="form-control {{ $errors->has('category_name') ? 'is-invalid' : '' }}" value="">

                                @if($errors->has('category_name'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('category_name') }}
                                </span>
                                @endif
                            </div>   
                            
                            <div class="form-group">
                                <label for="inputName">Category Image</label>
                                <input type="file" name="category_image" id="inputCategoryName" class="{{ $errors->has('category_image')  ? 'is-invalid' : '' }}" value="">

                                @if($errors->has('category_image'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('category_image') }}
                                </span>
                                @endif
                            </div> 
                            

                            <div class="col-12">
                                <a href="{{ route('admin.npocategories') }}" class="btn btn-secondary">Cancel</a>
                                <input type="submit" value="Add" class="btn btn-success float-right">
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection