@extends('layouts.app')

@section('header')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('app-assets/admin/plugins/jquery-ui/jquery-ui.css')}}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Coupon</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add Coupon</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">

                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data" action="{{ route('admin.createCoupon') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="inputName">Coupon Code</label>
                                <input type="text" name="coupon_code" id="inputCouponCode" class="form-control {{ $errors->has('coupon_code') ? 'is-invalid' : '' }}" value="{{ old('coupon_code') }}">

                                @if($errors->has('coupon_code'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('coupon_code') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Start Date</label>
                                <input type="text" name="coupon_start_date" id="inputCouponStartDate" class="form-control {{ $errors->has('coupon_start_date') ? 'is-invalid' : '' }}" value="{{ old('coupon_start_date') }}">

                                @if($errors->has('coupon_start_date'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('coupon_start_date') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">End Date</label>
                                <input type="text" name="coupon_end_date" id="inputCouponEndDate" class="form-control {{ $errors->has('coupon_end_date') ? 'is-invalid' : '' }}" value="{{ old('coupon_end_date') }}">

                                @if($errors->has('coupon_end_date'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('coupon_end_date') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="inputName">Value (Entries)</label>
                                <input type="number" name="coupon_value" id="inputCouponValue" class="form-control {{ $errors->has('coupon_value') ? 'is-invalid' : '' }}" value="{{ old('coupon_value') }}">

                                @if($errors->has('coupon_value'))
                                <span class="invalid-feedback">
                                    {{ $errors->first('coupon_value') }}
                                </span>
                                @endif
                            </div>

                            <div class="col-12">
                                <a href="{{ route('admin.coupons') }}" class="btn btn-secondary">Cancel</a>
                                <input type="hidden" name="coupon_status" value="active">
                                <input type="submit" value="Add" class="btn btn-success float-right">
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('footer')
<script>
    $(function() {
        $("#inputCouponStartDate").datepicker({
            minDate: 0,
            dateFormat: 'yy-mm-dd',
            onSelect: function(selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $("#inputCouponEndDate").datepicker("option", "minDate", dt);
            }
        });
        $("#inputCouponEndDate").datepicker({
            minDate: 0,
            dateFormat: 'yy-mm-dd',
            onSelect: function(selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() - 1);
                $("#inputCouponStartDate").datepicker("option", "maxDate", dt);
            }
        });

    });
</script>

@endsection