<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prizes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('prize_image_or_video_1')->nullable();
            $table->string('prize_image_or_video_2')->nullable();
            $table->date('prize_start_date');
            $table->date('prize_end_date');
            $table->string('prize_entry_id')->default(0);
            $table->string('prize_sponser_image')->nullable();
            $table->string('prize_entry_values');
            $table->integer('prize_nonprofit_id')->default(0);
            $table->mediumText('prize_title');
            $table->mediumText('prize_sub_title');            
            $table->longText('prize_body_text');
            $table->longText('prize_disclaimer_text');
            $table->enum('prize_type', ['champion', 'giveaway'])->default('giveaway');
            $table->enum('prize_status', ['active', 'inactive'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prizes');
    }
}
