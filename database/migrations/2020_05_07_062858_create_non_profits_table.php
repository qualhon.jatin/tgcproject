<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonProfitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('non_profits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('npo_first_name');
            $table->string('npo_last_name');
            $table->string('organization_name')->nullable();
            $table->string('npo_logo')->nullable();
            $table->string('npo_image_or_video')->nullable();
            $table->string('npo_contact')->nullable();
            $table->string('npo_email');
            $table->string('npo_phone');
            $table->string('password');
            $table->string('npo_city');
            $table->string('npo_state');
            $table->integer('npo_zipcode');
            $table->string('npo_website_url');
            $table->integer('npo_category');
            $table->string('npo_charity_navigator_rating');
            $table->longText('npo_mission_statement');
            $table->longText('npo_about_us');
            $table->longText('npo_celebrity_or_influential_supporters_include');
            $table->longText('npo_major_coporate_partners_include');
            $table->string('npo_affiliate_code')->nullable();
            $table->enum('npo_status', ['active', 'inactive'])->default('active');
            $table->string('remember_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('non_profits');
    }
}
