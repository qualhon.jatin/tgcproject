<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Npo extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization_name', 'npo_logo', 'npo_contact', 'npo_email', 'npo_phone' , 'password', 'npo_city', 'npo_state', 'npo_zipcode', 'npo_website_url', 'npo_category', 'npo_charity_navigator_rating', 'npo_mission_statement', 'npo_about_us', 'npo_events_and_programs', 'npo_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];
}
