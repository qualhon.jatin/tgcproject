<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class NonProfits extends Authenticatable
{
    use HasApiTokens,Notifiable;

    protected $guard = 'nonProfit';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization_name', 'npo_logo', 'npo_image_or_video', 'npo_first_name', 'npo_last_name', 'npo_email', 'npo_phone' , 'password', 'npo_city', 'npo_state', 'npo_zipcode', 'npo_website_url', 'npo_category', 'npo_charity_navigator_rating', 'npo_mission_statement', 'npo_about_us', 'npo_celebrity_or_influential_supporters_include', 'npo_major_coporate_partners_include', 'npo_affiliate_code', 'npo_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];
}
