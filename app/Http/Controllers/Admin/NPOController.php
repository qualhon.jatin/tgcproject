<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\NpoCategories;
use App\NonProfits;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;
use DB;
use App\Traits\UploadTrait; // Upload Image

class NPOController extends Controller
{

    use UploadTrait;
    /**
     * Only Authenticated users for "admin" guard 
     * are allowed.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show Admin User Listings.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.npo.index');
    }


    public function getNPOListings()
    {
        //$users = NonProfits::select('*');
        

        $users = \DB::table('non_profits')
        ->join('npo_categories', 'non_profits.npo_category', '=', 'npo_categories.id')
        ->select('non_profits.*', 'npo_categories.category_name')
        ->get();

        return DataTables::of($users)
            ->addColumn('action', 'admin.npo.action')
            ->addColumn('npo_first_name', function ($data) {
                return ($data->npo_first_name) . ' ' . ($data->npo_last_name);
            })
            ->rawColumns(['action', 'npo_first_name'])
            ->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $npoById = NonProfits::find($id);
        return view('admin.npo.view', compact('npoById'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $getNpoCategories = NpoCategories::all()->where('category_status', '=', 'active');
        return view('admin.npo.add')->with(compact('getNpoCategories'));
    }

    public function addNpo(Request $request)
    {

        $newNpo = new NonProfits;

        $validateUrl = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';

        $request->validate(
            [
                'organization_name' => 'required',
                'npo_first_name' => 'required',
                'npo_last_name' => 'required',
                'npo_email' => 'required|unique:non_profits',
                'npo_phone' => 'required|unique:non_profits',
                'password' => 'required',
                'npo_city' => 'required',
                'npo_state' => 'required',
                'npo_zipcode' => 'required',
                'npo_website_url' => 'required|regex:' . $validateUrl,
                'npo_category' => 'required',
                'npo_charity_navigator_rating' => 'required',
                'npo_mission_statement' => 'required',
                'npo_about_us' => 'required',
                'npo_celebrity_or_influential_supporters_include' => 'required',
                'npo_major_coporate_partners_include' => 'required',
                'npo_affiliate_code' => 'nullable',
                'npo_logo' => 'image|mimes:jpeg,png,jpg,gif',
                'npo_image_or_video' => 'mimes:jpeg,png,jpg,gif,mp4,mov,flv,m3u8,ts,3gp,avi,wmv | max:10000'
            ],
            [
                'organization_name.required' => 'Organization Name is required.',
                'npo_first_name.required' => 'First Name is required.',
                'npo_last_name.required' => 'Last Name is required.',
                'npo_email.required' => 'Email is required.',
                'npo_email.email' => 'Invalid Email Address.',
                'npo_phone.required' => 'Phone is required.',
                'npo_city.required' => 'City is required.',
                'npo_state.required' => 'State is required.',
                'npo_zipcode.required' => 'Zip Code is required.',
                'npo_website_url.required' => 'Website Url is required.',
                'npo_website_url.regex' => 'Invalid Website Url.',
                'npo_category.required' => 'Category is required.',
                'npo_charity_navigator_rating.required' => 'Charity Navigator Rating is required.',
                'npo_mission_statement.required' => 'Mission Statement is required.',
                'npo_about_us.required' => 'About Us is required.',
                'npo_celebrity_or_influential_supporters_include.required' => 'Celebrity or Influential Supporters include is required.',
                'npo_major_coporate_partners_include.required' => 'Major Corporate Partners / Sponsors Include is required.',
                'npo_logo.image' => 'Invalid Image.',
                'npo_image_or_video.mimes' => 'Invalid Image or Video.',
                'npo_image_or_video.max' => 'Maximum Image or Video size is 10 Mb.',
            ]
        );


        // Check if a profile image has been uploaded
        if ($request->has('npo_logo')) {
            // Get image file
            $image = $request->file('npo_logo');
            // Make a image name based on user name and current timestamp
            $name = 'npo_logo_' . time();
            // Define folder path
            $folder = '/app-assets/npo/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $newNpo->npo_logo = $filePath;
        }

        // Check if a NPO image or video has been uploaded
        if ($request->has('npo_image_or_video')) {
            // Get image file
            $image = $request->file('npo_image_or_video');
            // Make a image name based on user name and current timestamp
            $name = 'npo_image_or_video_' . time();
            // Define folder path
            $folder = '/app-assets/npo/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $newNpo->npo_image_or_video = $filePath;
        }

        $newNpo->organization_name = $request->input('organization_name');
        $newNpo->npo_first_name = $request->input('npo_first_name');
        $newNpo->npo_last_name = $request->input('npo_last_name');
        $newNpo->npo_email = $request->input('npo_email');
        $newNpo->npo_phone = $request->input('npo_phone');
        $newNpo->password = Hash::make($request->input('password'));
        $newNpo->npo_city = $request->input('npo_city');
        $newNpo->npo_state = $request->input('npo_state');
        $newNpo->npo_zipcode = $request->input('npo_zipcode');
        $newNpo->npo_category = $request->input('npo_category');
        $newNpo->npo_website_url = $request->input('npo_website_url');
        $newNpo->npo_charity_navigator_rating = $request->input('npo_charity_navigator_rating');
        $newNpo->npo_mission_statement = $request->input('npo_mission_statement');
        $newNpo->npo_about_us = $request->input('npo_about_us');
        $newNpo->npo_celebrity_or_influential_supporters_include = $request->input('npo_celebrity_or_influential_supporters_include');
        $newNpo->npo_major_coporate_partners_include = $request->input('npo_major_coporate_partners_include');
        $newNpo->npo_affiliate_code = $request->input('npo_affiliate_code');

        $newNpo->save();

        return redirect()
            ->intended(route('admin.npo'))
            ->with('status', $request->input('organization_name') . ' added successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getNpoCategories = NpoCategories::all()->where('category_status', '=', 'active');
        $npoById = NonProfits::find($id);
        return view('admin.npo.view', compact('npoById'))->with(compact('getNpoCategories'));
    }

    public function doEdit($id, Request $request)
    {

        

        $newNpo = NonProfits::find($id);

        $validateUrl = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';

       

        $request->validate(
            [
                'organization_name' => 'required',
                'npo_first_name' => 'required',
                'npo_last_name' => 'required',
                'npo_email' => 'required|email',
                'npo_phone' => 'required',                
                'password' => 'nullable',
                'npo_city' => 'required',
                'npo_state' => 'required',
                'npo_zipcode' => 'required',
                'npo_website_url' => 'required|regex:' . $validateUrl,
                'npo_category' => 'required',
                'npo_charity_navigator_rating' => 'required',
                'npo_mission_statement' => 'required',
                'npo_about_us' => 'required',
                'npo_celebrity_or_influential_supporters_include' => 'required',
                'npo_major_coporate_partners_include' => 'required',
                'npo_status' => 'required',
                'npo_affiliate_code' => 'nullable',
                'npo_logo' => 'image|mimes:jpeg,png,jpg,gif',
                'npo_image_or_video' => 'mimes:jpeg,png,jpg,gif,mp4,mov,flv,m3u8,ts,3gp,avi,wmv | max:10000'
            ],
            [
                'organization_name.required' => 'Organization Name is required.',
                'npo_first_name.required' => 'First Name is required.',
                'npo_last_name.required' => 'Last Name is required.',
                'npo_email.required' => 'Email is required.',
                'npo_email.email' => 'Invalid Email Address.',
                'npo_phone.required' => 'Phone is required.',
                'npo_city.required' => 'City is required.',
                'npo_state.required' => 'State is required.',
                'npo_zipcode.required' => 'Zip Code is required.',
                'npo_website_url.required' => 'Website Url is required.',
                'npo_website_url.regex' => 'Invalid Website Url.',
                'npo_category.required' => 'Category is required.',
                'npo_charity_navigator_rating.required' => 'Charity Navigator Rating is required.',
                'npo_mission_statement.required' => 'Mission Statement is required.',
                'npo_about_us.required' => 'About Us is required.',
                'npo_celebrity_or_influential_supporters_include.required' => 'Celebrity or Influential Supporters include is required.',
                'npo_major_coporate_partners_include.required' => 'Major Corporate Partners / Sponsors Include is required.',
                'npo_status.required' => 'Status is required.',
                'npo_logo.image' => 'Invalid Image.',
                'npo_image_or_video.mimes' => 'Invalid Image or Video.',
                'npo_image_or_video.max' => 'Maximum Image or Video size is 10 Mb.',
            ]
        );

        
        // Check if a NPO Logo has been uploaded
        if ($request->has('npo_logo')) {
            // Get image file
            $image = $request->file('npo_logo');
            // Make a image name based on user name and current timestamp
            $name = 'npo_logo_' . time();
            // Define folder path
            $folder = '/app-assets/npo/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $newNpo->npo_logo = $filePath;
        }

        // Check if a NPO image or video has been uploaded
        if ($request->has('npo_image_or_video')) {
            // Get image file
            $image = $request->file('npo_image_or_video');
            // Make a image name based on user name and current timestamp
            $name = 'npo_image_or_video_' . time();
            // Define folder path
            $folder = '/app-assets/npo/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $newNpo->npo_image_or_video = $filePath;
        }

        

        $newNpo->organization_name = $request->input('organization_name');
        $newNpo->npo_first_name = $request->input('npo_first_name');
        $newNpo->npo_last_name = $request->input('npo_last_name');
        $newNpo->npo_email = $request->input('npo_email');
        $newNpo->npo_phone = $request->input('npo_phone');
        if ($request->has('password')) { $newNpo->password = Hash::make($request->input('password')); }
        $newNpo->npo_city = $request->input('npo_city');
        $newNpo->npo_state = $request->input('npo_state');
        $newNpo->npo_zipcode = $request->input('npo_zipcode');
        $newNpo->npo_category = $request->input('npo_category');
        $newNpo->npo_website_url = $request->input('npo_website_url');
        $newNpo->npo_charity_navigator_rating = $request->input('npo_charity_navigator_rating');
        $newNpo->npo_mission_statement = $request->input('npo_mission_statement');
        $newNpo->npo_about_us = $request->input('npo_about_us');
        $newNpo->npo_celebrity_or_influential_supporters_include = $request->input('npo_celebrity_or_influential_supporters_include');
        $newNpo->npo_major_coporate_partners_include = $request->input('npo_major_coporate_partners_include');
        $newNpo->npo_status = $request->input('npo_status');
        $newNpo->npo_affiliate_code = $request->input('npo_affiliate_code');


        $newNpo->update();

        return redirect()
            ->intended(route('admin.npo'))
            ->with('status', $request->input('npo_email') . ' details updated.');
    }
}
