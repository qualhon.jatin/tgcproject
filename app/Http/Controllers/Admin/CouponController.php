<?php

namespace App\Http\Controllers\Admin;

use App\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\Admin\CouponRequest;

class CouponController extends Controller
{
    /**
     * Only Authenticated users for "admin" guard 
     * are allowed.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.coupons.index');
    }

    public function getCouponListings()
    {
        $getCoupons = Coupon::select('*');
        return DataTables::eloquent($getCoupons)
            ->addColumn('action', 'admin.coupons.action')
            ->rawColumns(['action'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coupons.create');
    }


    public function createCoupon(CouponRequest $createCouponRequest)
    {

        $newCoupon = new Coupon;

        $validateCoupon = $createCouponRequest->validated();        

        $newCoupon->coupon_code = $validateCoupon['coupon_code'];
        $newCoupon->coupon_start_date = $validateCoupon['coupon_start_date'];
        $newCoupon->coupon_end_date = $validateCoupon['coupon_end_date'];
        $newCoupon->coupon_value = $validateCoupon['coupon_value'];
        $newCoupon->save();

        return redirect()
            ->intended(route('admin.coupons'))
            ->with('status', $validateCoupon['coupon_code'] . ' coupon added successfully.');
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $couponById = Coupon::find($id);
        return view('admin.coupons.edit', compact('couponById'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCoupon(CouponRequest $createCouponRequest, $id)
    {
        $updateCoupon = Coupon::find($id);

        $validateCoupon = $createCouponRequest->validated(); 

        $updateCoupon->coupon_code = $validateCoupon['coupon_code'];
        $updateCoupon->coupon_start_date = $validateCoupon['coupon_start_date'];
        $updateCoupon->coupon_end_date = $validateCoupon['coupon_end_date'];
        $updateCoupon->coupon_value = $validateCoupon['coupon_value'];
        $updateCoupon->coupon_status = $validateCoupon['coupon_status'];
        $updateCoupon->update();

        return redirect()
            ->intended(route('admin.coupons'))
            ->with('status', $validateCoupon['coupon_code'] . ' coupon updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
