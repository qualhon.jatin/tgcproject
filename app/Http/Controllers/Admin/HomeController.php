<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;

class HomeController extends Controller
{
    /**
     * Only Authenticated users for "admin" guard 
     * are allowed.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show Admin Dashboard.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('admin.home');
    }


    public function settings(){
       
        return view('admin.settings');
    }

    public function updateSettings(Request $request){

        $user = Auth::user(); 

        $this->validator($request);

        $user->name = $request->input('name');

        if ($request->input('password') != "") {             
             $user->password = Hash::make($request->input('password'));            
        }
        
        $user->save();

        return redirect()
            ->intended(route('admin.settings'))
            ->with('status', 'Profile updated successfully.');
    }

    /**
     * Validate the form data.
     * 
     * @param \Illuminate\Http\Request $request
     * @return 
     */
    private function validator(Request $request)
    {
        //validation rules.
        $rules = [
            'name'    => 'required|string',
            'email'    => 'required|email|min:5|max:191',
            'password' => 'nullable|required_with:password_confirmation|confirmed|string|min:4|max:255',
            'password_confirmation' => 'nullable',
        ];

        //custom validation error messages.
        $messages = [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
        ];

        //validate the request.
        $request->validate($rules,$messages);
    }
}
