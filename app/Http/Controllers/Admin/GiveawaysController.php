<?php

namespace App\Http\Controllers\Admin;

use App\Prizes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Traits\UploadTrait; // Upload Image


class GiveawaysController extends Controller
{

    use UploadTrait;

    /**
     * Only Authenticated users for "admin" guard 
     * are allowed.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.giveaways.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.giveaways.create');
    }


    public function getGiveawaysListings()
    {
        $getPrizes = Prizes::select('*')->where('prize_type', '=', 'giveaway');
        return DataTables::eloquent($getPrizes)
            ->addColumn('action', 'admin.giveaways.action')
            ->rawColumns(['action'])
            ->toJson();
    }


    public function createGiveaway(Request $createGiveawayRequest)
    {

      
        $newPrize = new Prizes;
      

        $createGiveawayRequest->validate(
            [
                'prize_start_date' => 'required|date|before:prize_end_date',
                'prize_end_date' => 'required|date|after:prize_start_date',
                'prize_image_or_video_1' => 'mimes:jpeg,png,jpg,gif,mp4,mov,flv,m3u8,ts,3gp,avi,wmv | max:10000',
                'prize_image_or_video_2' => 'mimes:jpeg,png,jpg,gif,mp4,mov,flv,m3u8,ts,3gp,avi,wmv | max:10000',
                'prize_sponser_image' => 'image|mimes:jpeg,png,jpg,gif',
                'entry_value_array' => 'required',
                'prize_title' => 'required',
                'prize_sub_title' => 'required',
                'prize_body_text' => 'required',
                'prize_disclaimer_text' => 'required',
            ],
            [
                'prize_start_date.required' => 'Start Date is required.',
                'prize_end_date.required' => 'End Date is required.',
                'prize_end_date.date' => 'End Date is required.',
                'prize_start_date.date' => 'Start Date is required.',
                'prize_image_or_video_1.image' => 'Invalid Image.',
                'prize_image_or_video_1.mimes' => 'Invalid Image or Video.',
                'prize_image_or_video_1.max' => 'Maximum Image or Video size is 10 Mb.',
                'prize_image_or_video_2.image' => 'Invalid Image.',
                'prize_image_or_video_2.mimes' => 'Invalid Image or Video.',
                'prize_image_or_video_2.max' => 'Maximum Image or Video size is 10 Mb.',
                'prize_sponser_image.image' => 'Invalid Image.',
                'entry_value_array.required' => 'Entry Value is required.',
                'prize_title.required' => 'Heading is required.',
                'prize_sub_title.required' => 'Sub Title is required.',
                'prize_body_text.required' => 'Body Text is required.',
                'prize_disclaimer_text.required' => 'Disclaimer Text is required.',
            ]
        );

        

        // Check if a Prize image or video has been uploaded
        if ($createGiveawayRequest->has('prize_image_or_video_1')) {
            // Get image file
            $image = $createGiveawayRequest->file('prize_image_or_video_1');
            // Make a image name based on user name and current timestamp
            $name = 'prize_image_or_video_1_' . time();
            // Define folder path
            $folder = '/app-assets/prizes/giveaway/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $newPrize->prize_image_or_video_1 = $filePath;
        }

        // Check if a Prize image or video has been uploaded
        if ($createGiveawayRequest->has('prize_image_or_video_2')) {
            // Get image file
            $image = $createGiveawayRequest->file('prize_image_or_video_2');
            // Make a image name based on user name and current timestamp
            $name = 'prize_image_or_video_2_' . time();
            // Define folder path
            $folder = '/app-assets/prizes/giveaway/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $newPrize->prize_image_or_video_2 = $filePath;
        }

        // Check if a Prize image or video has been uploaded
        if ($createGiveawayRequest->has('prize_sponser_image')) {
            // Get image file
            $image = $createGiveawayRequest->file('prize_sponser_image');
            // Make a image name based on user name and current timestamp
            $name = 'prize_sponser_image_' . time();
            // Define folder path
            $folder = '/app-assets/prizes/giveaway/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $newPrize->prize_sponser_image = $filePath;
        }

        $newPrize->prize_start_date = $createGiveawayRequest->input('prize_start_date');
        $newPrize->prize_end_date = $createGiveawayRequest->input('prize_end_date');
        $newPrize->prize_title = $createGiveawayRequest->input('prize_title');
        $newPrize->prize_sub_title = $createGiveawayRequest->input('prize_sub_title');
        $newPrize->prize_body_text = $createGiveawayRequest->input('prize_body_text');
        $newPrize->prize_disclaimer_text = $createGiveawayRequest->input('prize_disclaimer_text');

        $newPrize->prize_entry_values = is_array($createGiveawayRequest->input('entry_value_array')) ? serialize($createGiveawayRequest->input('entry_value_array')) : $createGiveawayRequest->input('entry_value_array');

        
        $newPrize->save();

        return redirect()
            ->intended(route('admin.giveaways'))
            ->with('status', 'Giveaway added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $giveawayById = Prizes::find($id);
        return view('admin.giveaways.edit', compact('giveawayById'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateGiveaway(Request $updateGiveawayRequest, $id)
    {
        $updateGiveaway = Prizes::find($id);

        $updateGiveawayRequest->validate(
            [
                'prize_start_date' => 'required|date|before:prize_end_date',
                'prize_end_date' => 'required|date|after:prize_start_date',
                'prize_image_or_video_1' => 'mimes:jpeg,png,jpg,gif,mp4,mov,flv,m3u8,ts,3gp,avi,wmv | max:10000',
                'prize_image_or_video_2' => 'mimes:jpeg,png,jpg,gif,mp4,mov,flv,m3u8,ts,3gp,avi,wmv | max:10000',
                'entry_value_array' => 'required',
                'prize_title' => 'required',
                'prize_sub_title' => 'required',
                'prize_body_text' => 'required',
                'prize_disclaimer_text' => 'required',
            ],
            [
                'prize_start_date.required' => 'Start Date is required.',
                'prize_end_date.required' => 'End Date is required.',
                'prize_end_date.date' => 'End Date is required.',
                'prize_start_date.date' => 'Start Date is required.',
                'prize_image_or_video_1.image' => 'Invalid Image.',
                'prize_image_or_video_1.mimes' => 'Invalid Image or Video.',
                'prize_image_or_video_1.max' => 'Maximum Image or Video size is 10 Mb.',
                'prize_image_or_video_2.image' => 'Invalid Image.',
                'prize_image_or_video_2.mimes' => 'Invalid Image or Video.',
                'prize_image_or_video_2.max' => 'Maximum Image or Video size is 10 Mb.',
                'entry_value_array.required' => 'Entry Value is required.',
                'prize_title.required' => 'Heading is required.',
                'prize_sub_title.required' => 'Sub Title is required.',
                'prize_body_text.required' => 'Body Text is required.',
                'prize_disclaimer_text.required' => 'Disclaimer Text is required.',
            ]
        );


        // Check if a Prize image or video has been uploaded
        if ($updateGiveawayRequest->has('prize_image_or_video_1')) {
            // Get image file
            $image = $updateGiveawayRequest->file('prize_image_or_video_1');
            // Make a image name based on user name and current timestamp
            $name = 'prize_image_or_video_1_' . time();
            // Define folder path
            $folder = '/app-assets/prizes/giveaway/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $updateGiveaway->prize_image_or_video_1 = $filePath;
        }

        // Check if a Prize image or video has been uploaded
        if ($updateGiveawayRequest->has('prize_image_or_video_2')) {
            // Get image file
            $image = $updateGiveawayRequest->file('prize_image_or_video_2');
            // Make a image name based on user name and current timestamp
            $name = 'prize_image_or_video_2_' . time();
            // Define folder path
            $folder = '/app-assets/prizes/giveaway/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $updateGiveaway->prize_image_or_video_2 = $filePath;
        }

        // Check if a Prize image or video has been uploaded
        if ($updateGiveawayRequest->has('prize_sponser_image')) {
            // Get image file
            $image = $updateGiveawayRequest->file('prize_sponser_image');
            // Make a image name based on user name and current timestamp
            $name = 'prize_sponser_image_' . time();
            // Define folder path
            $folder = '/app-assets/prizes/giveaway/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $updateGiveaway->prize_sponser_image = $filePath;
        }

        $updateGiveaway->prize_start_date = $updateGiveawayRequest->input('prize_start_date');
        $updateGiveaway->prize_end_date = $updateGiveawayRequest->input('prize_end_date');
        $updateGiveaway->prize_title = $updateGiveawayRequest->input('prize_title');
        $updateGiveaway->prize_sub_title = $updateGiveawayRequest->input('prize_sub_title');
        $updateGiveaway->prize_body_text = $updateGiveawayRequest->input('prize_body_text');
        $updateGiveaway->prize_disclaimer_text = $updateGiveawayRequest->input('prize_disclaimer_text');
        $updateGiveaway->prize_status = $updateGiveawayRequest->input('prize_status');

        $updateGiveaway->prize_entry_values = is_array($updateGiveawayRequest->input('entry_value_array')) ? serialize($updateGiveawayRequest->input('entry_value_array')) : $updateGiveawayRequest->input('entry_value_array');

        $updateGiveaway->save();

        return redirect()
            ->intended(route('admin.giveaways'))
            ->with('status', ' Giveaway updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
