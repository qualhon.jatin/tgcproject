<?php

namespace App\Http\Controllers\Admin;


use App\NpoCategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Traits\UploadTrait; // Upload Image



class NpoCategoriesController extends Controller
{

    use UploadTrait;

    /**
     * Only Authenticated users for "admin" guard 
     * are allowed.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.npocategories.index');
    }

    public function getNPOCategoryListings()
    {
        $getNpoCategories = NpoCategories::select('*');
        return DataTables::eloquent($getNpoCategories)
            ->addColumn('category_image', function ($data) {
                return $this->getCategoryImage($data->category_image);
            })
            ->addColumn('action', 'admin.npocategories.action')
            ->rawColumns(['action', 'category_image'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.npocategories.create');
    }



    public function createNpoCategory(Request $request)
    {

        $newCategory = new NpoCategories;

        $request->validate([
            'category_name' => 'required',
            'category_image' => 'image|mimes:jpeg,png,jpg,gif',

        ]);


        // Check if a profile image has been uploaded
        if ($request->has('category_image')) {
            // Get image file
            $image = $request->file('category_image');
            // Make a image name based on user name and current timestamp
            $name = 'npo_category_' . time();
            // Define folder path
            $folder = '/app-assets/admin/categories/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $newCategory->category_image = $filePath;
        }

        $newCategory->category_name = $request->input('category_name');
        $newCategory->category_slug = $this->createSlug($request->input('category_name'));
        $newCategory->save();

        return redirect()
            ->intended(route('admin.npocategories'))
            ->with('status', $request->input('category_name') . ' category added successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $catById = NpoCategories::find($id);
        return view('admin.npocategories.edit', compact('catById'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateNpoCategory(Request $request, $id)
    {
        $updateCat = NpoCategories::find($id);

        $request->validate([
            'category_name' => 'required',
            'category_status' => 'required',
            'category_image' => 'image|mimes:jpeg,png,jpg,gif',

        ]);


        // Check if a profile image has been uploaded
        if ($request->has('category_image')) {
            // Get image file
            $image = $request->file('category_image');
            // Make a image name based on user name and current timestamp
            $name = 'npo_category_' . time();
            // Define folder path
            $folder = '/app-assets/admin/categories/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $updateCat->category_image = $filePath;
        }

        $updateCat->category_name = $request->input('category_name');
        $updateCat->category_status = $request->input('category_status');
        $updateCat->category_slug = $this->createSlug($request->input('category_name'));
        $updateCat->update();

        return redirect()
            ->intended(route('admin.npocategories'))
            ->with('status', $request->input('category_name') . ' category updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createSlug($str, $delimiter = '-')
    {

        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    public function getCategoryImage($category_image){
        $defaultImagePath = 'app-assets/admin/dist/img/noImage.png';
        if($category_image){
            return '<img class="images_avatar" src = "'.asset($category_image).'" >';
        }else{
            return '<img class="images_avatar" src = "'.asset($defaultImagePath ) .'" >';
        }

    }
}
