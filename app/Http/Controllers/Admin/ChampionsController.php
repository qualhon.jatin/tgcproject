<?php

namespace App\Http\Controllers\Admin;

use App\Prizes;
use Illuminate\Http\Request;
use App\NonProfits;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Traits\UploadTrait; // Upload Image

class ChampionsController extends Controller
{
    use UploadTrait;

    /**
     * Only Authenticated users for "admin" guard 
     * are allowed.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.champions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getNpos = NonProfits::all()->where('npo_status', '=', 'active');
        return view('admin.champions.create')->with(compact('getNpos'));
    }


    public function getChampionsListings()
    {
        $getPrizes = Prizes::select('*')->where('prize_type', '=', 'champion');
        return DataTables::eloquent($getPrizes)
            ->addColumn('action', 'admin.champions.action')
            ->rawColumns(['action'])
            ->toJson();
    }


    public function createChampion(Request $createChampionRequest)
    {

        $newPrize = new Prizes;


        $createChampionRequest->validate(
            [
                'prize_start_date' => 'required|date|before:prize_end_date',
                'prize_end_date' => 'required|date|after:prize_start_date',
                'prize_image_or_video_1' => 'mimes:jpeg,png,jpg,gif,mp4,mov,flv,m3u8,ts,3gp,avi,wmv | max:10000',
                'prize_image_or_video_2' => 'mimes:jpeg,png,jpg,gif,mp4,mov,flv,m3u8,ts,3gp,avi,wmv | max:10000',
                'prize_sponser_image' => 'image|mimes:jpeg,png,jpg,gif',
                'prize_nonprofit_id' => 'required',
                'entry_value_array' => 'required',
                'prize_title' => 'required',
                'prize_sub_title' => 'required',
                'prize_body_text' => 'required',
                'prize_disclaimer_text' => 'required',
                'prize_type' => 'required',
            ],
            [
                'prize_start_date.required' => 'Start Date is required.',
                'prize_end_date.required' => 'End Date is required.',
                'prize_end_date.date' => 'End Date is required.',
                'prize_start_date.date' => 'Start Date is required.',
                'prize_image_or_video_1.image' => 'Invalid Image.',
                'prize_image_or_video_1.mimes' => 'Invalid Image or Video.',
                'prize_image_or_video_1.max' => 'Maximum Image or Video size is 10 Mb.',
                'prize_image_or_video_2.image' => 'Invalid Image.',
                'prize_image_or_video_2.mimes' => 'Invalid Image or Video.',
                'prize_image_or_video_2.max' => 'Maximum Image or Video size is 10 Mb.',
                'prize_sponser_image.image' => 'Invalid Image.',
                'prize_nonprofit_id.required' => 'Non Profit Partner is required.',
                'entry_value_array.required' => 'Entry Value is required.',
                'prize_title.required' => 'Heading is required.',
                'prize_sub_title.required' => 'Sub Title is required.',
                'prize_body_text.required' => 'Body Text is required.',
                'prize_disclaimer_text.required' => 'Disclaimer Text is required.',
            ]
        );


        // Check if a Prize image or video has been uploaded
        if ($createChampionRequest->has('prize_image_or_video_1')) {
            // Get image file
            $image = $createChampionRequest->file('prize_image_or_video_1');
            // Make a image name based on user name and current timestamp
            $name = 'prize_image_or_video_1_' . time();
            // Define folder path
            $folder = '/app-assets/prizes/champion/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $newPrize->prize_image_or_video_1 = $filePath;
        }

        // Check if a Prize image or video has been uploaded
        if ($createChampionRequest->has('prize_image_or_video_2')) {
            // Get image file
            $image = $createChampionRequest->file('prize_image_or_video_2');
            // Make a image name based on user name and current timestamp
            $name = 'prize_image_or_video_2_' . time();
            // Define folder path
            $folder = '/app-assets/prizes/champion/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $newPrize->prize_image_or_video_2 = $filePath;
        }

        // Check if a Prize image or video has been uploaded
        if ($createChampionRequest->has('prize_sponser_image')) {
            // Get image file
            $image = $createChampionRequest->file('prize_sponser_image');
            // Make a image name based on user name and current timestamp
            $name = 'prize_sponser_image_' . time();
            // Define folder path
            $folder = '/app-assets/prizes/champion/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $newPrize->prize_sponser_image = $filePath;
        }

        $newPrize->prize_start_date = $createChampionRequest->input('prize_start_date');
        $newPrize->prize_end_date = $createChampionRequest->input('prize_end_date');
        $newPrize->prize_title = $createChampionRequest->input('prize_title');
        $newPrize->prize_sub_title = $createChampionRequest->input('prize_sub_title');
        $newPrize->prize_body_text = $createChampionRequest->input('prize_body_text');
        $newPrize->prize_disclaimer_text = $createChampionRequest->input('prize_disclaimer_text');
        $newPrize->prize_type = $createChampionRequest->input('prize_type');
        $newPrize->prize_nonprofit_id = $createChampionRequest->input('prize_nonprofit_id');

        $newPrize->prize_entry_values = is_array($createChampionRequest->input('entry_value_array')) ? serialize($createChampionRequest->input('entry_value_array')) : $createChampionRequest->input('entry_value_array');

        $newPrize->save();

        return redirect()
            ->intended(route('admin.champions'))
            ->with('status', 'Champion added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $championById = Prizes::find($id);
        $getNpos = NonProfits::all()->where('npo_status', '=', 'active');
        return view('admin.champions.edit', compact('championById'))->with(compact('getNpos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateChampion(Request $updateChampionRequest, $id)
    {
        $updateChampion = Prizes::find($id);

        $updateChampionRequest->validate(
            [
                'prize_start_date' => 'required|date|before:prize_end_date',
                'prize_end_date' => 'required|date|after:prize_start_date',
                'prize_image_or_video_1' => 'mimes:jpeg,png,jpg,gif,mp4,mov,flv,m3u8,ts,3gp,avi,wmv | max:10000',
                'prize_image_or_video_2' => 'mimes:jpeg,png,jpg,gif,mp4,mov,flv,m3u8,ts,3gp,avi,wmv | max:10000',
                'entry_value_array' => 'required',
                'prize_nonprofit_id' => 'required',
                'prize_title' => 'required',
                'prize_sub_title' => 'required',
                'prize_body_text' => 'required',
                'prize_disclaimer_text' => 'required',
            ],
            [
                'prize_start_date.required' => 'Start Date is required.',
                'prize_end_date.required' => 'End Date is required.',
                'prize_end_date.date' => 'End Date is required.',
                'prize_start_date.date' => 'Start Date is required.',
                'prize_image_or_video_1.image' => 'Invalid Image.',
                'prize_image_or_video_1.mimes' => 'Invalid Image or Video.',
                'prize_image_or_video_1.max' => 'Maximum Image or Video size is 10 Mb.',
                'prize_image_or_video_2.image' => 'Invalid Image.',
                'prize_image_or_video_2.mimes' => 'Invalid Image or Video.',
                'prize_image_or_video_2.max' => 'Maximum Image or Video size is 10 Mb.',
                'prize_nonprofit_id.required' => 'Non Profit Partner is required.',
                'entry_value_array.required' => 'Entry Value is required.',
                'prize_title.required' => 'Heading is required.',
                'prize_sub_title.required' => 'Sub Title is required.',
                'prize_body_text.required' => 'Body Text is required.',
                'prize_disclaimer_text.required' => 'Disclaimer Text is required.',
            ]
        );


        // Check if a Prize image or video has been uploaded
        if ($updateChampionRequest->has('prize_image_or_video_1')) {
            // Get image file
            $image = $updateChampionRequest->file('prize_image_or_video_1');
            // Make a image name based on user name and current timestamp
            $name = 'prize_image_or_video_1_' . time();
            // Define folder path
            $folder = '/app-assets/prizes/champion/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $updateChampion->prize_image_or_video_1 = $filePath;
        }

        // Check if a Prize image or video has been uploaded
        if ($updateChampionRequest->has('prize_image_or_video_2')) {
            // Get image file
            $image = $updateChampionRequest->file('prize_image_or_video_2');
            // Make a image name based on user name and current timestamp
            $name = 'prize_image_or_video_2_' . time();
            // Define folder path
            $folder = '/app-assets/prizes/champion/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $updateChampion->prize_image_or_video_2 = $filePath;
        }

        // Check if a Prize image or video has been uploaded
        if ($updateChampionRequest->has('prize_sponser_image')) {
            // Get image file
            $image = $updateChampionRequest->file('prize_sponser_image');
            // Make a image name based on user name and current timestamp
            $name = 'prize_sponser_image_' . time();
            // Define folder path
            $folder = '/app-assets/prizes/champion/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $updateChampion->prize_sponser_image = $filePath;
        }

        $updateChampion->prize_start_date = $updateChampionRequest->input('prize_start_date');
        $updateChampion->prize_end_date = $updateChampionRequest->input('prize_end_date');
        $updateChampion->prize_title = $updateChampionRequest->input('prize_title');
        $updateChampion->prize_sub_title = $updateChampionRequest->input('prize_sub_title');
        $updateChampion->prize_body_text = $updateChampionRequest->input('prize_body_text');
        $updateChampion->prize_disclaimer_text = $updateChampionRequest->input('prize_disclaimer_text');
        $updateChampion->prize_status = $updateChampionRequest->input('prize_status');
        $updateChampion->prize_nonprofit_id = $updateChampionRequest->input('prize_nonprofit_id');

        $updateChampion->prize_entry_values = is_array($updateChampionRequest->input('entry_value_array')) ? serialize($updateChampionRequest->input('entry_value_array')) : $updateChampionRequest->input('entry_value_array');

        $updateChampion->save();

        return redirect()
            ->intended(route('admin.champions'))
            ->with('status', ' Champion updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
