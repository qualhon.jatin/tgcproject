<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\Http\Controllers\Controller;
use App\User;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Only Authenticated users for "admin" guard 
     * are allowed.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show Admin User Listings.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('admin.user.index');
    }


    public function getUserListings(){
        $users = User::select('*')->where('user_role', '=', 'subscriber');
        return DataTables::eloquent($users)
            ->addColumn('action','admin.user.action')
            ->rawColumns(['action'])
            ->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userById = User::find($id);
        return view('admin.user.view',compact('userById'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userById = User::find($id);
        return view('admin.user.view',compact('userById'));
    }

    public function doEdit($id, UpdateUserRequest $updateUserRequest){
        $validated = $updateUserRequest->validated();
        $user = User::find($id);
        $userData = [
            'email' => $validated['email'],
            'first_name' => $validated['first_name'],
            'last_name' => $validated['last_name'],
            'name' => $validated['first_name']. ' ' .$validated['last_name'],
            'user_status' => $validated['user_status']
        ];

        //echo "<pre>"; print_r($userData); echo "</pre>"; die('sfds');

        if($updateUserRequest->has('password')){
            $userData['password'] = Hash::make($validated['password']);
        }

        $user->update($userData);       

        return redirect()
                ->intended(route('admin.user'))
                ->with('status','User '.$validated['email'].' details updated.');
    }
}
