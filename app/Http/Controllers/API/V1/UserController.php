<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Auth;
use Illuminate\Foundation\Auth\Allusers as Authenticatable;
use Laravel\Passport\PassportServiceProvider;
use Illuminate\Support\Str;
use Mail;

class UserController extends BaseController
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'password' => ['required', 'string', 'min:8', 'max:50']
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input = $request->all();
        $userExists = User::where('email', '=', $input['email'])->first();
        if (empty($userExists)) {

            
            $user = new User();
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->name = "";
            $user->save();

            $input = $request->all();
            Auth::attempt($input);
            $user = Auth::user();
            $success['accountType'] = $user->user_role;
            $success['response'] = Config('constants.SUCCESSFULLY_REGISTERED');
            $token = $user->createToken('MyApp')->accessToken;
            return response()->json($success, 200)->header('x-auth-token', $token);
        } else {

            $error['response'] = Config('constants.USER_ALREADY_EXIST');
            return response()->json($error, 401);
        }
    }
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'password' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input = $request->all();

        if (Auth::attempt($input)) {

            $user = Auth::user();
            $success['accountType'] = $user->user_role;
            $success['response'] = Config('constants.LOGIN_SUCCESSFULL');
            $token = $user->createToken('MyApp')->accessToken;
            return response()->json($success, 200)->header('x-auth-token', $token);
        } else {
            $error['response'] = Config('constants.INVALID_USER_DETAILS');
            return response()->json($error, 401);
        }
    }
    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input = $request->all();
        $userExists = User::where('email', '=', $input['email'])->first();
        if (!empty($userExists)) {
            $token = Str::random(60);

            $update_token['remember_token'] = $token;
            user::where('email', '=', $input['email'])->update($update_token);
            // $forgot_password_link = Config('constants.FORGOT_PASSWORD_LINK') . $token;
            $forgot_password_link = '13.239.180.63/tgc/#/user/reset-password?token='.$token;

            $data['token'] = $token;
            $data['email'] = $input['email'];
            $data['name'] = $userExists['name'];
            $data['forgot_password_link'] = $forgot_password_link;

            Mail::send('api.forgot_password_email', $data, function ($message) use ($data) {
                $message->from('no-reply@thegreatconnect.org', 'TGC');
                $message->to($data['email'], $data['name']);
                $message->subject('Forgot password!');
            });

            if (Mail::failures()) {
                $error['response'] = Config('constants.SOMETHING_WENT_WRONG');
                return response()->json($error, 401);
            } else {
                $success['response'] = Config('constants.EMAIL_SENT_SUCCESSFULLY');
                return response()->json($success, 200);
            }
        } else {
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }
    }
    public function logout(Request $request)
    {
        if (Auth::check()) {
            Auth::user()->token()->revoke();
            $success['response'] = Config('constants.LOGOUT_SUCCESSFULLY');;
            return response()->json($success, 200);
        } else {
            $error['response'] = Config('constants.INVALID_AUTH_TOKEN');
            return response()->json($error, 401);
        }
    }
    public function verifyAuthToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'verifyToken' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input = $request->all();
        $userInfo = User::where('remember_token', '=', $input['verifyToken'])->first();
        if (!empty($userInfo)) {
            $success['response'] = Config('constants.SUCCESS');
            $success['user_id'] = $userInfo['id'];

            $update_token['remember_token'] = "";
            User::where('id', '=', $userInfo['id'])->update($update_token);

            return response()->json($success, 200);
        } else {
            $error['response'] = Config('constants.TOKEN_EXPIRED');
            return response()->json($error, 401);
        }
    }
    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'password' => ['required', 'string', 'min:8', 'max:50']
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input = $request->all();
        $userInfo = User::where('id', '=', $input['user_id'])->first();
        if (!empty($userInfo)) {

            $update_data['password'] = Hash::make($input['password']);

            $response = USER::where('id', '=', $input['user_id'])->update($update_data);
            if ($response) {

                $data['email'] = $userInfo['email'];
                $data['name'] = $userInfo['name'];

                Mail::send('api.reset_password_success', $data, function ($message) use ($data) {
                    $message->from('no-reply@thegreatconnect.org', 'TGC');
                    $message->to($data['email'], $data['name']);
                    $message->subject('Password reset successfully!');
                });

                if (Mail::failures()) {
                    $error['response'] = Config('constants.SOMETHING_WENT_WRONG');
                    //return response()->json($error, 401);
                }

                $success['response'] = Config('constants.PASSWORD_RESET_SUCCESSFULLY');
                return response()->json($success, 200);
            } else {
                $error['response'] = Config('constants.SOMETHING_WENT_WRONG');
                return response()->json($error, 401);
            }
        } else {
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }
    }
    public function getUserDetails(Request $request){
        $userInfo = Auth::user();
        if(!empty($userInfo)){
            $success['response'] = Config('constants.SUCCESS');
            $success['user_details'] = $userInfo;
            return response()->json($success, 200);
        }else{
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }
    }
    public function updateUserProfile(Request $request){
        $input = $request->all();

        if(!empty($input['password'])){
            $validator = Validator::make($request->all(), [
                'first_name' => ['required', 'string'],
                'last_name' => ['required', 'string'],
                'name' => ['required', 'string'],
                'password' => ['required', 'string', 'min:8', 'max:50']
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'first_name' => ['required', 'string'],
                'last_name' => ['required', 'string'],
                'name' => ['required', 'string']
            ]);
        }
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $userInfo = Auth::user();

        $update_data['first_name'] = $input['first_name'];
        $update_data['last_name'] = $input['last_name'];
        $update_data['name'] = $input['name'];
        if(!empty($input['password'])){
            $update_data['password'] = Hash::make($input['password']);
        }
        $response = USER::where('id', '=', $userInfo->id)->update($update_data);
        if ($response) {
            $success['response'] = Config('constants.PROFILE_UPDATED_SUCCESSFULLY');
            return response()->json($success, 200);
        } else {
            $error['response'] = Config('constants.SOMETHING_WENT_WRONG');
            return response()->json($error, 401);
        }
    }
}
