<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Prizes;

class PrizeController extends Controller
{

    /*
        Function name : getPrizes
        Function Purpose : Get all prizes list
        Created by : Jatin
        Created on : 5 Oct 2020
    */

    public function getPrizes(){
        $prizes = Prizes::where('prize_type', '=', 'giveaway')->orderBy('id','Desc')->get();
        $prize_result = [];
        foreach($prizes as $prize){
            $prize['prize_image_or_video_1'] = !empty($prize['prize_image_or_video_1']) ? url($prize['prize_image_or_video_1']) : "";
            $prize_result[] = $prize;
        }
        $success['response'] = Config('constants.SUCCESS');
        $success['data'] = $prizes;
        return response()->json($success, 200);
    }

    /*
        Function name : getPrizeDetail
        Function Purpose : Get prize detail by id
        Created by : Jatin
        Created on : 5 Oct 2020
    */
    public function getPrizeDetail($id){

        $prizeDetail = Prizes::where('prize_type', '=', 'giveaway')->where('id',$id)->first();

        if(!empty($prizeDetail)){
            $prizeDetail['prize_image_or_video_1'] = !empty($prizeDetail['prize_image_or_video_1']) ?  url($prizeDetail['prize_image_or_video_1']) : "";
            $prizeDetail['prize_image_or_video_2'] = !empty($prizeDetail['prize_image_or_video_2']) ? url($prizeDetail['prize_image_or_video_2']) : "";
            $prizeDetail['prize_sponser_image'] = !empty($prizeDetail['prize_image_or_video_1']) ? url($prizeDetail['prize_sponser_image']) : "";

            $success['response'] = Config('constants.SUCCESS');
            $success['data'] = $prizeDetail;

            return response()->json($success, 200);
        }else{
            $success['response'] = "error";
            $success['data'] = "";

            return response()->json($success, 401);
        }
    }

    /*
        Function name : getPrizesExcludeId
        Function Purpose : Get prize detail by exclude specific data
        Created by : sourabh
        Created on : 8 Oct 2020
    */  

    public function getPrizesExcludeId(Request $request_prize_id){
        $prizes_exclude_data = Prizes::where('prize_type', '=', 'giveaway')
                            ->whereNotIn('id',[$request_prize_id->prize_id])
                            ->orderBy('id','Desc')->get();
        $prize_result = [];
        foreach($prizes_exclude_data as $prize){
            $prize['prize_image_or_video_1'] = url($prize['prize_image_or_video_1']);
            $prize_result[] = $prize;
        }
        $success['response'] = Config('constants.SUCCESS');
        $success['data'] = $prizes_exclude_data;
        return response()->json($success, 200);
    }
}
