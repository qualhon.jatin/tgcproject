<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Carts;
use App\Orders;
use App\Coupon;
use App\Order_details;
use App\Payment_details;
use Auth;
use Validator;
use Stripe;

class PaymentController extends BaseController
{
    public function makePayment(Request $request){
        $userInfo = Auth::user();
        if(!empty($userInfo)){
            $validator = Validator::make($request->all(), [
                'stripe_token' => 'required',
                'npo_id' => 'required'
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }

            $cartItems = Carts::select('id','user_id','giveaway_id','giveaway_title','giveaway_image','entry_amount','total_entries','quantity')->where('user_id',$userInfo->id)->get();
            

            if(!empty($cartItems)){

                
                $checkCoupon = Coupon::where('coupon_code',$request->coupon_code)->first();

                $coupon_id = null;
                $coupon_code = null;
                $coupon_entry_value = null;
                if(!empty($checkCoupon)){
                    $coupon_id = $checkCoupon->id;
                    $coupon_code = $checkCoupon->coupon_code;
                    $coupon_entry_value = $checkCoupon->coupon_value;
                }

                $amount = 0;
                foreach($cartItems as $item){
                    $amount = $amount + ($item->entry_amount * $item->quantity);
                }
                $order = new Orders;
                $order->user_id = $userInfo->id;
                $order->amount = $amount;
                $order->npo_id = $request->npo_id;
                $order->coupon_id = $coupon_id;
                $order->coupon_code = $coupon_code;
                $order->coupon_entry_value = $coupon_entry_value;
                $order->order_status = "0";
                $order->payment_status = "0";
                $order->save();
                
                if(!empty($order->id)){
                    foreach($cartItems as $item){
                        $orderDetails = new Order_details;
                        $orderDetails->user_id = $userInfo->id;
                        $orderDetails->order_id = $order->id;
                        $orderDetails->giveaway_id = $item->giveaway_id;
                        $orderDetails->entry_amount = $item->entry_amount;
                        $orderDetails->total_entries = $item->total_entries;
                        $orderDetails->quantity = $item->quantity;
                        $orderDetails->save();
                    }
                    Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                    $charge = Stripe\Charge::create ([
                        "amount" => $amount * 100,
                        "currency" => "usd",
                        "source" => $request->stripe_token,
                        "description" => "Test payment from codehunger.in." 
                    ]);
                    if ($charge->paid == true) {

                        $paymentDetails = new Payment_details;
                        $paymentDetails->user_id = $userInfo->id;
                        $paymentDetails->order_id = $order->id;
                        $paymentDetails->amount = $amount;
                        $paymentDetails->stripe_token = $request->stripe_token;
                        $paymentDetails->txn_id = $charge['id'];
                        $paymentDetails->payment_status = "1";
                        $paymentDetails->save();

                        $order = Orders::find($order->id);
                        $order->payment_status = "1";
                        $order->update();

                        $cartItems = Carts::where('user_id',$userInfo->id)->delete();

                        $success['transactionDetails'] = $charge;
                        $success['response'] = "success";
                        return response()->json($success, 200);
                    } else {

                        $paymentDetails = new Payment_details;
                        $paymentDetails->user_id = $userInfo->id;
                        $paymentDetails->order_id = $order->id;
                        $paymentDetails->amount = $amount;
                        $paymentDetails->stripe_token = $request->stripe_token;
                        $paymentDetails->txn_id = null;
                        $paymentDetails->payment_status = "0";
                        $paymentDetails->save();

                        $error['response'] = 'Failed to make your payment. Please try again later.';
                        return response()->json($error, 401);
                    }
                }else{
                    $error['response'] = "Something went wrong with your order";
                    return response()->json($error, 401);
                }
                

                // echo "<pre>";
                // print_r($charge);
            }else{
                $error['response'] = "Something went wrong with your order";
                return response()->json($error, 401);
            }

            
        }else{
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }
        
    }
}
