<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\NonProfits;
use App\NpoCategories;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Auth;
use Illuminate\Foundation\Auth\Allusers as Authenticatable;
use Laravel\Passport\PassportServiceProvider;
use Illuminate\Support\Str;
use Mail;
use App\Traits\UploadTrait; // Upload Image

class NonProfitController extends BaseController
{

    use UploadTrait;

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'password' => ['required', 'string', 'min:8', 'max:50']
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input = $request->all();
        $userExists = NonProfits::where('npo_email', '=', $input['email'])->first();
        if (empty($userExists)) {

            $user = new NonProfits();
            $user->npo_email = $request->email;
            $user->password = Hash::make($request->password);
            $user->npo_first_name = "";
            $user->npo_last_name = "";
            $user->npo_phone = "";
            $user->npo_city = "";
            $user->npo_state = "";
            $user->npo_zipcode = 0;
            $user->npo_website_url = "";
            $user->npo_category = 0;
            $user->npo_charity_navigator_rating = "";
            $user->npo_mission_statement = "";
            $user->npo_about_us = "";
            $user->npo_celebrity_or_influential_supporters_include = "";
            $user->npo_major_coporate_partners_include = "";
            // $user->npo_category = "";
            // $user->npo_category = "";
            $user->save();
            $success['response'] = Config('constants.SUCCESSFULLY_REGISTERED');
            return response()->json($success, 200);
        } else {

            $error['response'] = Config('constants.USER_ALREADY_EXIST');
            return response()->json($error, 401);
        }
    }
    
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'password' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        // $input = $request->all();
        $input['npo_email'] = $request->email;
        $input['password'] = $request->password;
        config(['auth.guards.nonProfit.driver'=>'session']);
        if (Auth::guard('nonProfit')->attempt($input)) {
            $user = Auth::guard('nonProfit')->user();
            $success['accountType'] = "npo";
            $success['response'] = Config('constants.LOGIN_SUCCESSFULL');
            $token = $user->createToken('MyApp')->accessToken;
            return response()->json($success, 200)->header('x-auth-token', $token);
        } else {
            $error['response'] = Config('constants.INVALID_USER_DETAILS');
            return response()->json($error, 401);
        }
    }
    public function getNonProfitDetails(Request $request){
        $userInfo = Auth::guard('nonProfit')->user();
        if(!empty($userInfo)){
            $success['response'] = Config('constants.SUCCESS');
            $success['user_details'] = $userInfo;
            return response()->json($success, 200);
        }else{
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }
    }
    public function logout(Request $request)
    {
        if (Auth::guard('nonProfit')->check()) {
            Auth::guard('nonProfit')->user()->token()->revoke();
            $success['message'] = 'Successfully logout!';
            return response()->json($success, 200);
        } else {
            $error['response'] = Config('constants.INVALID_AUTH_TOKEN');;
            return response()->json($error, 401);
        }
    }
    public function updateNonProfitProfile(Request $request){
        $input = $request->all();

        if(!empty($input['password'])){
            $validator = Validator::make($request->all(), [
                'first_name' => ['required', 'string'],
                'last_name' => ['required', 'string'],
                'organization_name' => ['required', 'string'],
                'password' => ['required', 'string', 'min:8', 'max:50']
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'first_name' => ['required', 'string'],
                'last_name' => ['required', 'string'],
                'organization_name' => ['required', 'string']
            ]);
        }
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $userInfo = Auth::guard('nonProfit')->user();
 
        $update_data['npo_first_name'] = $input['first_name' ];
        $update_data['npo_last_name'] = $input['last_name'];
        $update_data['organization_name'] = $input['organization_name'];
        if(!empty($input['password'])){
            $update_data['password'] = Hash::make($input['password']);
        }
        $response = NonProfits::where('id', '=', $userInfo->id)->update($update_data);
        if ($response) {
            $success['response'] = Config('constants.PROFILE_UPDATED_SUCCESSFULLY');
            return response()->json($success, 200);
        } else {
            $error['response'] = Config('constants.SOMETHING_WENT_WRONG');
            return response()->json($error, 401);
        }
    }
    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input = $request->all();
        $userExists = NonProfits::where('npo_email', '=', $input['email'])->first();
        if (!empty($userExists)) {
            $token = Str::random(60);

            $update_token['remember_token'] = $token;
            NonProfits::where('npo_email', '=', $input['email'])->update($update_token);
            // $forgot_password_link = Config('constants.FORGOT_PASSWORD_LINK_NONPROFIT') .$token;
            $forgot_password_link = '13.239.180.63/tgc/#/nonprofit/reset-password?token='.$token;
            $data['token'] = $token;
            $data['email'] = $input['email'];
            $data['name'] = $userExists['npo_first_name'];
            $data['forgot_password_link'] = $forgot_password_link;

            Mail::send('api.forgot_password_email', $data, function ($message) use ($data) {
                $message->from('no-reply@thegreatconnect.org', 'TGC');
                $message->to($data['email'], $data['name']);
                $message->subject('Forgot password!');
            });

            if (Mail::failures()) {
                $error['response'] = Config('constants.SOMETHING_WENT_WRONG');
                return response()->json($error, 401);
            } else {
                $success['response'] = Config('constants.EMAIL_SENT_SUCCESSFULLY');
                return response()->json($success, 200);
            }
        } else {
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }
    }
    public function verifyAuthToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'verifyToken' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input = $request->all();
        $userInfo = NonProfits::where('remember_token', '=', $input['verifyToken'])->first();
        if (!empty($userInfo)) {
            $success['response'] = Config('constants.SUCCESS');
            $success['user_id'] = $userInfo['id'];

            $update_token['remember_token'] = "";
            NonProfits::where('id', '=', $userInfo['id'])->update($update_token);

            return response()->json($success, 200);
        } else {
            $error['response'] = Config('constants.TOKEN_EXPIRED');
            return response()->json($error, 401);
        }
    }

    public function imageUploadtoBucket(Request $request){  
        
        $input = $request->all();

        // Check if a NPO image or video has been uploaded
        if ($request->has('npo_image_or_video')) {
            // Get image file
            $image = $request->file('npo_image_or_video');
            // Make a image name based on user name and current timestamp
            $name = 'npo_image_or_video_' . time();
            // Define folder path
            $folder = '/app-assets/npo/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $success['response'] = Config('constants.SUCCESS');
            $success['fileLocation'] = $filePath;
            $success['fileMimeType'] = $input['fileType'];
            return response()->json($success, 200);
        }
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'password' => ['required', 'string', 'min:8', 'max:50']
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input = $request->all();
        $userInfo = NonProfits::where('id', '=', $input['user_id'])->first();
        if (!empty($userInfo)) {

            $update_data['password'] = Hash::make($input['password']);

            $response = NonProfits::where('id', '=', $input['user_id'])->update($update_data);
            if ($response) {

                $data['email'] = $userInfo['npo_email'];
                $data['name'] = $userInfo['npo_first_name'];

                Mail::send('api.reset_password_success', $data, function ($message) use ($data) {
                    $message->from('no-reply@thegreatconnect.org', 'TGC');
                    $message->to($data['email'], $data['name']);
                    $message->subject('Password reset successfully!');
                });

                if (Mail::failures()) {
                    $error['response'] = Config('constants.SOMETHING_WENT_WRONG');
                    //return response()->json($error, 401);
                }

                $success['response'] = Config('constants.PASSWORD_RESET_SUCCESSFULLY');
                return response()->json($success, 200);
            } else {
                $error['response'] = Config('constants.SOMETHING_WENT_WRONG');
                return response()->json($error, 401);
            }
        } else {
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }
    }
    public function getNPOCategories(){
        $npo_categories = NpoCategories::select('id','category_name','category_slug','category_image')->where('category_status','active')->orderBy('id','Desc')->take(8)->get();

        $result = [];
        foreach($npo_categories as $category){
            $category['category_image'] = url($category['category_image']);
            $result[] = $category;
        }
        $success['response'] = Config('constants.SUCCESS');
        $success['data'] = $npo_categories;
        return response()->json($success, 200);
    }
    public function getNPOList(){
        $npo_list = NonProfits::select('id','organization_name','npo_logo','npo_category')->where('npo_status','active')->get();
        $success['response'] = Config('constants.SUCCESS');
        $success['data'] = $npo_list;
        return response()->json($success, 200);
    }
    public function getNPOListByCategory(Request $request){
        
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $category_id = $request->category_id;
        $npo_list = NonProfits::select('id','organization_name','npo_logo','npo_category')->where('npo_category',$category_id)->where('npo_status','active')->get();
        $success['response'] = Config('constants.SUCCESS');
        $success['data'] = $npo_list;
        return response()->json($success, 200);
    }
}
