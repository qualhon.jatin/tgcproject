<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Carts;
use App\Prizes;
use App\Coupon;
use App\Orders;
use App\Order_details;
use Auth;
use Validator;
use Stripe;

class CartController extends BaseController
{
    public function addCart(Request $request){
        $userInfo = Auth::user();
        if(!empty($userInfo)){
            $validator = Validator::make($request->all(), [
                'giveaway_id' => 'required',
                'entry_amount' => 'required',
                'quantity' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            

            $checkCart = Carts::where('user_id',$userInfo->id)->where('giveaway_id',$request->giveaway_id)->where('entry_amount',$request->entry_amount)->first();

            if(!empty($checkCart)){
                $cart = Carts::find($checkCart->id);
                $cart->quantity = $cart->quantity + $request->quantity;
                $cart->update();
            }else{
                $prizeDetail = Prizes::where('id',$request->giveaway_id)->first();
            
                $cartValues = new Carts;
                $cartValues->user_id = $userInfo->id;
                $cartValues->giveaway_id = $request->giveaway_id;
                $cartValues->giveaway_title = $prizeDetail->prize_title;
                $cartValues->giveaway_image = $prizeDetail->prize_sponser_image;
                $cartValues->entry_amount = $request->entry_amount;
                $cartValues->total_entries = $request->entry_amount * 10;
                $cartValues->quantity = $request->quantity;
                $cartValues->save();
            }
            $success['response'] = "Product added successfully";
            return response()->json($success, 200);
        }else{
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }

    }
    public function getCartItems(){
        $userInfo = Auth::user();
        if(!empty($userInfo)){
            $cartItems = Carts::select('id','user_id','giveaway_id','giveaway_title','giveaway_image','entry_amount','total_entries','quantity')->where('user_id',$userInfo->id)->get();
            $success['response'] = "success";

            $carts = [];
            foreach($cartItems as $item){
                $item['giveaway_image'] =   !empty($item['giveaway_image']) ? url($item['giveaway_image']) : "";
                $cart[] = $item;

            }
            $success['data'] = $cartItems;
            return response()->json($success, 200);
        }else{
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }
    }
    public function deleteCartItem(Request $request){
        $userInfo = Auth::user();
        if(!empty($userInfo)){
            $validator = Validator::make($request->all(), [
                'giveaway_id' => 'required',
                'entry_amount' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $checkCart = Carts::where('user_id',$userInfo->id)->where('giveaway_id',$request->giveaway_id)->where('entry_amount',$request->entry_amount)->first();
            if(!empty($checkCart)){
                $cart = Carts::find($checkCart->id);
                $cart->delete();
                $success['response'] = "Item deleted successfully";
                return response()->json($success, 200);
            }else{
                $error['response'] = "Item doesn't found";
                return response()->json($error, 401);
            }
        }else{
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }

    }
    public function updateCartItem(Request $request){
        $userInfo = Auth::user();
        if(!empty($userInfo)){
            $validator = Validator::make($request->all(), [
                'giveaway_id' => 'required',
                'entry_amount' => 'required',
                'quantity' => 'required'
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            if($request->quantity < 1){
                $error['response'] = "Item quantity must be greater than zero.";
                return response()->json($error, 401);
            }
            $checkCart = Carts::where('user_id',$userInfo->id)->where('giveaway_id',$request->giveaway_id)->where('entry_amount',$request->entry_amount)->first();
            if(!empty($checkCart)){
                $cart = Carts::find($checkCart->id);
                $cart->quantity = $request->quantity;
                $cart->update();
                $success['response'] = "Item updated successfully";
                return response()->json($success, 200);
            }else{
                $error['response'] = "Item doesn't found";
                return response()->json($error, 401);
            }
        }else{
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }

    }
    public function applyCoupon(Request $request){
        $userInfo = Auth::user();
        if(!empty($userInfo)){
            $validator = Validator::make($request->all(), [
                'coupon_code' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
           
            $checkCoupon = Coupon::where('coupon_code',$request->coupon_code)->first();
            if(!empty($checkCoupon)){
                $success['response'] = "Coupon found";
                $success['data'] = $checkCoupon->coupon_value;
                return response()->json($success, 200);
            }else{
                $error['response'] = "Coupon code doesn't exist";
                return response()->json($error, 401);
            }
        }else{
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }
    }
    public function getOrderHistory(){
        $userInfo = Auth::user();
        
        if(!empty($userInfo)){
            $orders = Orders::select('orders.created_at as order_date','prizes.prize_title','order_details.total_entries','order_details.entry_amount','non_profits.organization_name','payment_details.txn_id')->join('order_details','orders.id','=','order_details.order_id')->join('prizes','prizes.id','=','order_details.giveaway_id')->join('non_profits','non_profits.id','=','orders.npo_id')->join('payment_details','payment_details.order_id','=','orders.id')->where('orders.user_id',$userInfo->id)->where('orders.payment_status','1')->get();
            $success['response'] = "success";
            $success['data'] = $orders;
            return response()->json($success, 200);
        }else{
            $error['response'] = Config('constants.USER_NOT_FOUND');
            return response()->json($error, 401);
        }
    }
   
}
