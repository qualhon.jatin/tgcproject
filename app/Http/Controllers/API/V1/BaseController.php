<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Controllers\Controller as Controller;

class BaseController extends Authenticatable
{
    /**
    * success response method.
    *
    * @return \IlluminaMyAppte\Http\Response
    */
    public function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'response' => $message,
        ];
        
        return response()->json($response, 200);
    }


    /**
        * return error response.
        *
        * @return \Illuminate\Http\Response
        */
    public function sendError($error, $errorMessages = [], $code = 401)
    {
        $response = [
            'success' => false,
            'response' => $error,
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
}
