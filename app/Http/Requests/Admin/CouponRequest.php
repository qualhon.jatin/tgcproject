<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'coupon_code' => 'required',
            'coupon_start_date' => 'required|date|before:coupon_end_date',
            'coupon_end_date' => 'required|date|after:coupon_start_date',
            'coupon_value' => 'required',
            'coupon_status' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'coupon_code.required' => 'Coupon Code is required.',
            'coupon_start_date.required' => 'Start Date is required.',
            'coupon_start_date.before' => 'Invalid Start Date.',
            'coupon_start_date.date' => 'Invalid Start Date.',
            'coupon_end_date.required' => 'End Date is required.',
            'coupon_end_date.after' => 'Invalid End Date.',
            'coupon_end_date.date' => 'Invalid End Date.',
            'coupon_value.required' => 'Value (Entries) is required.',
            'coupon_status.required' => 'Status is required.'
        ];
    }
}
