<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:2|max:20',
            'last_name' => 'required|min:2|max:20',
            'email' => 'required|email|min:5|max:191',
            'password' => 'nullable|required_with:password_confirmation|confirmed|string|min:4|max:255',
            'password_confirmation' => 'nullable',
            'user_status' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'First Name is required.',
            'last_name.required'  => 'Last Name is required.',
            'email.required'  => 'Email is required.',
        ];
    }
}
