<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Prizes extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prize_image_or_video_1', 'prize_image_or_video_2', 'prize_start_date', 'prize_end_date', 'prize_entry_values', 'prize_nonprofit_id','prize_entry_values', 'prize_sponser_image', 'prize_title' , 'prize_sub_title', 'prize_body_text', 'prize_disclaimer_text', 'prize_type', 'prize_status'
    ];
}
