<?php

return [
    "SUCCESSFULLY_REGISTERED" => "Thank you for registering on The Great Connect. You can now login in your account.",
    "USER_ALREADY_EXIST" => "Email address already registered. Try another one.",
    "LOGIN_SUCCESSFULL" => "Login successfull. redirecting...",
    "INVALID_USER_DETAILS" => "Invalid Email or Password.",
    "USER_NOT_FOUND" => "Invalid User Details.",
    "FORGOT_PASSWORD_LINK" => "13.239.180.63/tgc/#/user/reset-password?token=",
    "FORGOT_PASSWORD_LINK_NONPROFIT" => "13.239.180.63/tgc/#/nonprofit/reset-password?token=",
    "SOMETHING_WENT_WRONG" => "Something went wrong! Please try again later.",
    "EMAIL_SENT_SUCCESSFULLY" => "Please check your email for password reset instructions.",
    "INVALID_AUTH_TOKEN" => "Invalid Token or Token Expired.",
    "TOKEN_EXPIRED" => "Invalid Token.",
    "SUCCESS" => "success",
    "PASSWORD_RESET_SUCCESSFULLY" => "Password Updated Successfully.",
    "LOGOUT_SUCCESSFULLY" => "Logout Successfully.",
    "PROFILE_UPDATED_SUCCESSFULLY" => "Profile Updated Successfully.",
    
];